#!/bin/sh

#  RunBowtie.sh
#  
#
#  Created by Ivory on 5/23/14.
#  Taken directly from runBowtie.sh that Ann used for cytokinin ChIPseq
#

#Run program from inside the fastqFiles foler,
#by giving the path to the scripts folder.

#before running, make sure all files are unzipped.

# launch process to prevent sleep and move it to the background
pmset noidle &
# save the process ID
PMSETPID=$!
#set up the path so that you can reach the bowtie and samtools functions.
export PATH=.:../software:../software/bowtie-1.0.1:../software/samtools-0.1.19:$PATH

## Us this command in the index folder to build the index before running alignment.
#bowtie-build A_thaliana_Jun_2009.fa A_thaliana_Jun_2009

cmd="bowtie -S -m 1 ../index/A_thaliana_Jun_2009 "
now=$(date +"%m_%d_%Y")

echo $now > ../scripts/err.log-RunBowtie-$now
errLog= 2>>../scripts/err.log-RunBowtie-$now #still don't know why it was an issue to just include $errLog at the end of each command.


for f in $(ls *.fastq)
do
prefix=${f%.fastq}
echo "doing $prefix alignment"
echo $prefix >>../scripts/err.log-RunBowtie-$now
#
$cmd $f > ../bamFiles/$prefix.sam 2>>../scripts/err.log-RunBowtie-$now
#
cd ../bamFiles
echo "converting $prefix to bam"
#
samtools view -bS $prefix.sam > $prefix.bam 2>>../scripts/err.log-RunBowtie-$now
samtools sort $prefix.bam $prefix.sorted 2>>../scripts/err.log-RunBowtie-$now
mv $prefix.sorted.bam $prefix.bam 2>>../scripts/err.log-RunBowtie-$now
samtools index $prefix.bam 2>>../scripts/err.log-RunBowtie-$now
cd ../fastqFiles
done

echo "All done."

# kill pmset so the computer can sleep if it wants to
kill $PMSETPID


### End