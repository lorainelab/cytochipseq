---
title: "Expression data from study E-MTAB-1596"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction

In this micro array study, Arabidopsis thaliana seedlings were grown in constant light for 14 days. Seedlings were treated by immersion with either 10 micro molar BA (benzyladenine) or a DMSO vehicle control for 60 minutes. RNA was collected from the shoots.  The study included three replicates for each treatment.

To my knowledge, this study has not been published outside of the meta analysis by Bhagrava et al. 2013.

In this mark down, we will copute expression values and a set of differentially expressed genes based on the CEL files aquired from ArrayExpress under accession E-MTAB-1596.

## Questions

 * How many genes were assayed by the array?
 * How many genes were detected and analyzed?
 * How many genes were different between the treatment and the control?

# Load data and tools

Packages we will use most.
```{r}
#source("https://bioconductor.org/biocLite.R")
#biocLite("affy")
#biocLite("limma")
suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(limma))
```

Download the CEL files from Array Express.
```{r}
system("wget -P CEL ftp://ftp.ebi.ac.uk/pub/databases/microarray/data/experiment/MTAB/E-MTAB-1596/E-MTAB-1596.raw.1.zip")
system("unzip -d CEL -u CEL/E-MTAB-1596.raw.1.zip")
system("rm CEL/E-MTAB-1596.raw.1.zip")
```

Read in the samples.txt file describing which samples are in each treatment group.
```{r}
MySamples<-read.AnnotatedDataFrame("data/Samples.txt", header=TRUE, sep="\t", row.names=1)
```


Get the gene to probe-set associtions.
```{r}
fname="../ps_info.txt.gz"
if (!file.exists(fname)){
  url="http://www.igbquickload.org/quickload/A_thaliana_Jun_2009/ps_info.txt.gz"
  system(paste("wget -O", fname, url))
}
ps.info=read.delim(fname)
```


# Set up the data as an Affy object

## Set up meta data

The phenodata is in the MySamples data frame.
```{r}
pData(MySamples)
```

## Read in CEL files

Use the filename column from MySamples to link the treatment information to the individual CEL files as they are read in.
```{r}
MyData<-ReadAffy(filenames=MySamples$filename, phenoData=MySamples, verbose=TRUE, celfile.path="CEL")
sampleNames(MyData)<-row.names(pData(MySamples))
MyData
```

R should have figured out that the CEL files came from the ATH1 array, and it did.  We also know that this array should have 22810 ids, and it looks like it does.

## Quality control

Just as a quality check, we can look at the images for each CEL file.
```{r}
par(mfrow=c(2,3), mai=c(0,.1,.4,0))
#This takes some time.
image(MyData)
```

And we can look at a histogram of each one, showing the frequency of probset values.  They should all look about the same.
```{r}
hist(MyData)
```

In this set, the overal intensity is much brighter in some files than in others. This is why normalization is a must.

# Analysis

## Convert to expression set

We need to convert the data into normalized expression values.  We will use the rma() method whihch uses the Robust Multi-Array Average whcih is described in this paper:
Irizarry, RA, Hobbs, B, Collin, F, Beazer-Barclay, YD, Antonellis, KJ, Scherf, U, Speed, TP (2003) Exploration, Normalization, and Summaries of High Density Oligonucleotide Array Probe Level Data. Biostatistics .Vol. 4, Number 2: 249-264

Note that this expression measure is given to you in log base 2 scale. This differs from most of the other expression measure methods.

The new object will be an expression set, so we'll call it eset.
```{r}
eset<-rma(MyData)
```

## Set up comparison
We need to tell R what factors to use in this comparison.  Make a design matrix giving the relationship between the samples.  In this case, there is only one factor (treatment) so the design is very simple.
```{r}
myFactors<-MySamples$treatment
myFactors<-factor(myFactors)
design<-model.matrix(~0+myFactors)
colnames(design)<-levels(myFactors)
```

Now we'll tell R that we want to analyze the values for all the probsets in these microarrays (data that we've put in to the object "eset") based on the relationships they have in the design matrix (which we called "design").  This will give us an MArrayLM object, which will have expression values for each type of sample withe the replicates within each type combined.
The $coefficients portion of the object fit is the average expression value for each probset in each group.
```{r}
fit = lmFit(eset,design)
head(fit$coefficients)
```

In addition to laying out how samples relate (which ones are in a group togher) we need to explain how we want to compare them by creating a contrast matrix.
```{r}
contrast.matrix=makeContrasts(cyto = BA - DMSO,levels=design)
contrast.matrix
```
We are only working with two groups and one comparison, so our matrix is pretty simple.

## Compare groups

Now we get to actually make the contrast using the ebays method.

The empircal Bayes method is described in:
Smyth, G. K. (2004). Linear models and empirical Bayes methods for assessing differential expression in microarray experiments. Statistical Applications in Genetics and Molecular Biology, Volume 3, Article 3. 
http://www.bepress.com/sagmb/vol3/iss1/art3

```{r}
myContrasts = contrasts.fit(fit,contrast.matrix)
myContrasts = eBayes(myContrasts)
head(myContrasts$coefficients)
```

At this point, the object myContrasts has the results many statistical tests.  We want to know which of those results were significant. 
The decideTesets function is designed to take the output of the eBayes function. 
```{r}
pval=0.05 # the p-value threshold for significance.
lfc=0 # the minimum log2 fold change for significance.
basicResults=decideTests(myContrasts, p.value=pval, lfc=lfc)
TopCyto=topTable(myContrasts, coef="cyto", number=nrow(eset), p.value=pval, lfc=lfc)
table(basicResults)
```

 * With these criteria, we have `r nrow(TopCyto)` differentially expressed ids.
 * `r sum(TopCyto$logFC > 0)` up-regulated ids. and
 * `r sum(TopCyto$logFC < 0)` down-regulated ids.
 
## Link ids to genes

To make our results meaningful, we need to translate this probset information into information about genes.

Merge the gene to probe-set associtions with probe sets from our results.
```{r}
CytoEffect=merge(ps.info, TopCyto, by.x="ps", by.y="row.names")
```

Were all of the probe sets from our results linked to at least one gene?

There `r nrow(TopCyto)` probe sets in our initial results, and `r length(unique(CytoEffect$ps))` of them were linked to one or more genes, for a total of `r length(unique(CytoEffect$AGI))` genes.

AT4G34588 and AT4G34590 are both associated with the probesite id "253245_at".  Looking at these two genes in IGB, AT4G34588 and AT4G34590 have identical start and stop positions, but different coding regions. So they are distinct genes but there is no way to know from the mRNA which of the two proteins would be produced. Looking at the *ATH1 probe sets* track, the probset was clearly made for the AT4G34590 coding region.

# Save results to a file

We can rearrange the columns so the information we want to see first comes first. Set up an ideal order for the columns and rows.
```{r}
rowOrder=order(CytoEffect$logFC, decreasing=T)
colOrder=colnames(CytoEffect)[c(4:5, 8,9,11,12, 1:3, 6)]
colOrder
CytoEffectFile=CytoEffect[rowOrder,colOrder]
```

```{r}
baseName="results/SchallerArray2013"
fname=paste0(baseName,"_pval", pval,".txt")
write.table(CytoEffectFile, file=fname, 
            row.names=FALSE, col.names=TRUE, sep='\t', quote=F)
```
This is saved as `r fname`.  It includes

 * `r nrow(CytoEffectFile)` rows
 * `r length(unique(CytoEffectFile$ps))` probe sets
 * `r length(unique(CytoEffectFile$AGI))` genes
 * `r length(unique(CytoEffectFile$AGI[CytoEffectFile$logFC>0]))` up-regulated genes (based on `r length(unique(CytoEffectFile$ps[CytoEffectFile$logFC>0]))` unique probesets)
 * `r length(unique(CytoEffectFile$AGI[CytoEffectFile$logFC<0]))` down-regulated genes (based on `r length(unique(CytoEffectFile$ps[CytoEffectFile$logFC<0]))` unique probesets)


Save an additional file that has all of the results in case we want to try other thresholds elsewhere.
```{r}
# get results in the same form as before but without imposing any thresholds
allIds=topTable(myContrasts, coef="cyto", number=nrow(eset), p.value=1, lfc=0)
# Also tack on the expression values for individual samples
allIds=merge(allIds, exprs(eset), by="row.names")
# link this full set to genes
allIds=merge(ps.info, allIds, by.x="ps", by.y="Row.names")
# use the same column order as before, with any additional columns coming at the end
allIds=allIds[,c(colOrder, colnames(allIds)[!colnames(allIds) %in% colOrder])]
# order rows by AGI to make it easier to look up genes
ord=order(as.character(allIds$AGI))
allIds=allIds[ord,]
#save file
fnameAll=paste0(baseName,"All", ".txt")
write.table(allIds, file=fnameAll, 
            row.names=FALSE, col.names=TRUE, sep='\t', quote=F)
system(paste("gzip -f", fnameAll))
```
The file `r fname` contains the results for all probe sets.
It includes `r nrow(allIds)` rows with `r length(unique(allIds$AGI))` genes, represented by `r length(unique(allIds$ps))` probe sets.


# Conclusions
**How many genes were assayed by the array?**
 
 The array includes 22810 probe sets. Only `r length(unique(ps.info$ps))` of those represent genes. With each probe set representing one or more genes, our results could include up to some `r length(unique(ps.info$AGI))` genes.
 
**How many genes were detected and analyzed?**

We have values for `r length(unique(allIds$AGI))` unique genes (based on `r length(unique(allIds$ps))` probe sets).

**How many genes were different between the treatment and the control?**

There were `r length(unique(CytoEffect$AGI))` genes (based on `r length(unique(CytoEffect$ps))` probe sets) that had a p-value equal to less than `r pval` and a log fold change of at least `r lfc`.

***Session Info***
```{r}
sessionInfo()
```