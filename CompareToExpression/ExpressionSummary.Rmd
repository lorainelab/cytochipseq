---
title: "Expression summary for genes near peaks"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction

At this point, there is a complete markdown for some or all of the expression data sets. Now, we want to look a the different expression sets *together*. In this markdown, we'll choose a small number of columns to represent each data set so we can see if they tell a similar story. 

## Questions

 * How many genes were in more than one expression set?  Were these regulated in the same direction in the different data sets we considered?
 * How far are peaks from the TSS of a DE gene? Does direction of regulation make a difference?

# Assemble data

In bringing the data togehter, we want to keep ALL the genes that are near peaks and that have some expression change, but in the end we may or may not keep all peaks or all genes from the original peaks-to-genes list.  

## Read chip-seq peaks with genes
```{r}
source("../src/ProjectVariables.R")
fname="../GeneAssignment/results/ConsensusPeaks_ClosestGene_limit1500bp.txt" 
pg=read.delim(fname,header=T,sep='\t',as.is=T)
genesToKeep=c()
df=pg #make a main data frame to add all the info into
```
This set includes 

 * `r nrow(pg)` peak-to-gene relationships,
 * `r length(unique(pg$geneName))` genes, and
 * `r length(unique(pg$peakName))` peaks.

## Add on Apurva's RNA-Seq data
The markdown called **CompareToRNASeq** goes into greater detail in looking at genes that were near peaks and differentially expressed in response to cytokinin in the RNA-Seq study done by Bhargrava et al. 2013. Here, we will only use a portion of the results from that markdown.

Read in the results of the RNA-seq comparison.
```{r}
FDR=RNAseqFDR()
fnameRSAB='../ExternalDataSets/Cytokinin.tsv.gz'
rs.WS=read.delim(fnameRSAB,sep='\t',header=T,quote='',as.is=T) #rs for RnaSeq and WS for Whole Seedling
rs.WS=rs.WS[rs.WS$fdr<=FDR,]
genesToKeep=c(genesToKeep,unique(as.character(rs.WS$locus)))
```
This list of `r nrow(rs.WS)` rows includes `r length(unique(rs.WS$locus))` genes.

We only considered genes that were differentially expressed with an fdr of `r FDR` or better.  Now we'll summarize those genes with just their log2 fold change and their expression level in the treated condition.
```{r}
rs.WS=rs.WS[,c("locus", "logFC", "tr.ave")]
names(rs.WS)=c("gene", "RNAseqWholeSeedling.Log2FC", "RNAseqWholeSeedling.treated.ave")
```

Add this to the main data frame.
```{r}
df=merge(df, rs.WS, by.x="geneName", by.y="gene", all.x=T)
```
We filled in `r sum(!is.na(df$RNAseqWholeSeedling.Log2FC))` rows with the rna-seq info for `r length(unique(df$geneName[!is.na(df$RNAseqWholeSeedling.Log2FC)]))` genes.

## Add on Tracy's 2014 RNA-seq data
Read in the root results of the 2014 RNA-seq experiment.
```{r}
fnameRo="../ExternalDataSets/WtRo.txt.gz"
ro=read.table(fnameRo, sep='\t', header=T, quote="\"")
ro=ro[ro$fdr<=FDR,]
genesToKeep=c(genesToKeep,unique(as.character(ro$gene)))
```
This list of `r nrow(ro)` rows includes `r length(unique(ro$gene))` genes, based on an FDR of `r FDR`.

```{r}
fnameSh="../ExternalDataSets/WtSh.txt.gz"
sh=read.table(fnameSh, sep='\t', header=T, quote="\"")
sh=sh[sh$fdr<=FDR,]
genesToKeep=c(genesToKeep,unique(as.character(sh$gene)))
```
This list of `r nrow(sh)` rows includes `r length(unique(sh$gene))` genes, based on an FDR of `r FDR`.

Only keep the log 2 Fold change information.
```{r}
ro=ro[,c("gene", "logFC")]
names(ro)=c("gene", "RNAseqRoot.Log2FC")
sh=sh[,c("gene", "logFC")]
names(sh)=c("gene", "RNAseqShoot.Log2FC")
```

Add root RNAseq data to the main data frame.
```{r}
df=merge(df, ro, by.x="geneName", by.y="gene", all.x=T)
```
We filled in `r sum(!is.na(df$RNAseqRoot.Log2FC))` rows with the rna-seq info for `r length(unique(df$geneName[!is.na(df$RNAseqRoot.Log2FC)]))` genes.


Add shoot RNAseq data to the main data frame.
```{r}
df=merge(df, sh, by.x="geneName", by.y="gene", all.x=T)
```
We filled in `r sum(!is.na(df$RNAseqShoot.Log2FC))` rows with the rna-seq info for `r length(unique(df$geneName[!is.na(df$RNAseqShoot.Log2FC)]))` genes.

## Add on 2008 micro array data
The markdowns called **CompareToDifferentiallyRegulatedGenes** and **CompareToMoreDRgenes** go into greater detail in looking at genes that were near peaks and differentially expressed in response to cytokinin in the micro array study done by Argyros et al. 2008. Here, we will only use a portion of the results from that markdown.

Read the results for the full array comparison (CompareToMoreDRgenes).
```{r}
fnameDR="results/TripMutantFullArrayDiffRegAllGenes.txt.gz"
marray2008=read.table(fnameDR, sep='\t', header=T, quote="")
# recall that the last several columns had the information about differences.
catsOfInterest=c(names(marray2008)[22:ncol(marray2008)])
# only keep rows that had some kind of difference.
keep=apply(marray2008[,catsOfInterest], 1, any)
marray2008=marray2008[keep,]
# now count the genes
genesToKeep=c(genesToKeep,unique(as.character(marray2008$AGI)))
```
This list of `r nrow(marray2008)` rows includes `r length(unique(marray2008$AGI))` genes that were in one of our categories of interest. (Categories include `r catsOfInterest`.)

We can use this study in two ways: we can look at genes that are regulated by cytokinin in the wild type, and we can look at genes that are different between the wild type and the mutant.
```{r}
WtRegGenes=sum(marray2008[,catsOfInterest[9]] |
                 marray2008[,catsOfInterest[10]])
MuDiffFromWtGenes=sum(apply(marray2008[catsOfInterest[1:8]], 1, any))
```

`r WtRegGenes` of the genes on the list are here because they were regulated by cytokinin in the treated condition (based on fold change, see CompareToMoreDRgenes).

`r MuDiffFromWtGenes` of the genes on the list are here because they were different between the mutant and the wild type (iether in one of the conditions or in their response to the treatment, see CompareToMoreGenes).

To make a concise summary of this data, we'll create new columns:

 * the ratio of the wild type fold change to the mutant fold change.
 * the ratio of the wild type expression to the mutant expression in the treated condition.
 * the fold change for the wild type expressed as the log2 fold change

I don't generally like to use "wild typ / mutant", I think "mutant / wild type" makes much more sense.  But in this case, the wild type is the positive (type-Bs are present) and the mutant is the negative (type-Bs are absent) and this direction makes bigger numbers have a more similar meaning to bigger numbers in the other columns.  If we take the log of the ratios, then we will have an easier time comparisn the extent of data in each direction.

```{r}
marray2008$Sch2008.FC.WildTypeToMutantRatio = log2(marray2008$WT.BA.DMSO. /
                                                    marray2008$Mutant.BA.DMSO.)
marray2008$Sch2008.Treated.WildTypeToMutantRatio = log2(marray2008$WT.BA / marray2008$Mutant.BA)
marray2008$Sch2008.Log2FC = log2(marray2008$WT.BA.DMSO.)
marray2008=marray2008[,c("AGI", 
                         "Sch2008.Log2FC", 
                         "Sch2008.FC.WildTypeToMutantRatio",
                         "Sch2008.Treated.WildTypeToMutantRatio")]
```
Note that not every row had valid values for both the wild type and the mutant. Any time the information is not valid (NA), there will not be a new value to add to the main data frame. 

Add this to the main data frame.
```{r}
df=merge(df, marray2008, by.x="geneName", by.y="AGI", all.x=T)
```
We filled in `r sum(!is.na(df$Sch2008.FC.WildTypeToMutantRatio))` rows with the micro array info for `r length(unique(df$geneName[!is.na(df$Sch2008.FC.WildTypeToMutantRatio)]))` genes.

## Add on the Schaller 2013 micro array data
Read the results for the full array (BA treated vs DMSO treated).
```{r}
fnameMA="../ExternalDataSets/E-MTAB-1596/results/SchallerArray2013All.txt.gz"
ma2=read.table(fnameMA, sep='\t', header=T, quote="")
ma2=ma2[ma2$adj.P.Val<=FDR,]
genesToKeep=c(genesToKeep,unique(as.character(ma2$geneName)))
```
This list of `r nrow(ma2)` rows includes `r length(unique(ma2$AGI))` genes.

Limit this table to some good summary info:

 * The log2 fold change of the BA treated relative to the DMSO treated.

```{r}
ma2=ma2[,c("AGI", "logFC")]
names(ma2)=c("geneName", "Sch2013.Log2FC")
```


Add this to the main data frame.
```{r}
df=merge(df, ma2, by="geneName", all.x=T)
```
We filled in `r sum(!is.na(df$Sch2013.Log2FC))` rows with the micro array info for `r length(unique(df$geneName[!is.na(df$Sch2013.Log2FC)]))` genes.

## Add on golden list data

Recall that the golden list is a series of genes that were consistently regulated by cytokinin in several micro array studies. The Argyros et al 2008 study was one of those studies, so aggreement between them is not additive evidence of anything.
The micro array study has value here as a complete set of genes from one study. The golden list is a comparatively conservative list, that gives us insight into consisten trends.

Read in the golden list. For more details about the comparing the peaks data to the golden list, see the **CompareToGoldenList** markdown.
```{r}
gl=read.csv("../ExternalDataSets/TheGoldenList.csv", header=F)
names(gl)=c("gene", "symbol", "direction", "numArrays")
genesToKeep=c(genesToKeep,unique(as.character(gl$gene)))
```
This set of `r nrow(gl)` rows describes `r length(unique(gl$gene))` genes.

The golden list only really has two values for each gene: up or down, and the number of arrays.  We can represent that with a single column, and use positive or negative values to convey the direction of the consistent change.
```{r}
gl$GoldenList = gl$numArrays * ifelse(gl$direction=="Up", 1, -1)
gl=gl[,c("gene", "GoldenList")]
```

```{r}
df=merge(df, gl, by.x="geneName", by.y="gene", all.x=T)
```
We filled in `r sum(!is.na(df$GoldenList))` rows with the micro array info for `r length(unique(df$geneName[!is.na(df$GoldenList)]))` genes.

# Consider all the data together

## How many genes were in more than one expression set?

Of all the genes that had any support from any of the expression data sets, how many had support from more than one?
```{r}
supportPerGene=table(table(genesToKeep))
supportPerGene
```
There were `r supportPerGene[1]` genes with support from `r names(supportPerGene)[1]` data set, ..., `r supportPerGene[max(names(supportPerGene))]` genes with support from `r max(names(supportPerGene))` datasets.

Of all the genes that were assigned to peaks, how many had support from more than one expression set?
```{r}
supportPerPeakGene=table(table(genesToKeep[genesToKeep %in% pg$geneName]))
supportPerPeakGene
```

Look at the gene overlap between expression sets. This includes both up- and down-regulated genes and genes that were not on the peaks-to-genes list.
```{r fig.width=3, fig.height=3}
#library(Vennerable)
library(gplots)
par(mar=c(0,0,0,0))
venn(list(RNAseq=unique(rs.WS$gene),
       ma2008=unique(as.character(marray2008$AGI)),
       ma2013=unique(as.character(ma2$geneName))))
venn(list(RNAseq=unique(rs.WS$gene),
       root=unique(as.character(ro$gene)),
       shoot=unique(as.character(sh$gene))))
venn(list(RNAseq=unique(rs.WS$gene),
       ma2008=unique(marray2008$AGI),
       ma2013=unique(as.character(marray2008$AGI)),
       GL=unique(as.character(gl$gene))))
```

## Were genes regulated the same way in multiple data sets?

###Draw heat map
For the heat map, we will only use genes that were near peaks.

For the heatmap, each column will need to be on a similar scale.  Put all of the values on a scale from -1 to 1.  Make sure all negative values are still negative and positive values are still possitive.  
```{r fig.height=10}
dfEx=df[,c("geneName",
           "RNAseqShoot.Log2FC","RNAseqRoot.Log2FC","RNAseqWholeSeedling.Log2FC",
           "Sch2008.Log2FC","Sch2013.Log2FC",
           "GoldenList","Sch2008.FC.WildTypeToMutantRatio","Sch2008.Treated.WildTypeToMutantRatio")]
# make names shorter
names(dfEx)=gsub("WildTypeToMutant", "", names(dfEx))

rnames <- dfEx[,1]                            # assign labels in column 1 to "rnames"
dfEx <- data.matrix(dfEx[,2:ncol(dfEx)])  # transform column 2-5 into a matrix
rownames(dfEx) <- rnames     

#normalize each column to have a range between -1 and 1.
shrinkRange=function(x){
  maxRange=max(abs(x), na.rm=T)
  ret=x/maxRange
  return(ret)
}
for (col in 1:ncol(dfEx)){
  dfEx[,col]=shrinkRange(dfEx[,col])
}

# set up colors.
my_palette <- c(colorRampPalette(c("blue", "lightgreen"))(n = 98),
                "#FFFFFF", "#FFFFFF",
                colorRampPalette(c("orange", "red"))(n = 99))
# col_breaks=c(seq(-1,0,length=100),
#              seq(0,1,length=100))

#Some genes may be on the list without having a value in the summary columns in the data frame.  Get rid of those. We wanted them in our results files, but not here.
notNA=apply(dfEx, 1, function(x){any(!is.na(x))})
dfEx=dfEx[notNA,]

# order by the sum of all non-na values in each row.
dfEx=dfEx[order(rowSums(dfEx, na.rm=T), decreasing=T),]

# Draw heat map
hm<-heatmap.2(dfEx,
  main = "Expression support for genes near peaks",
  trace="none",         # turns off trace lines inside the heat map
  margins =c(12,1),     # widens margins around plot
  col=my_palette,       # use on color palette defined earlier 
  #breaks=col_breaks,   # never did figure out why this breaks stuff
  breaks=200,
  dendrogram="none",     
  na.rm=T,              # ignore NA values
  Rowv=F, Colv=F)       # turn off column and row clustering
```

### Add direction column

```{r}
indStuds=c("Sch2008.Log2FC", "Sch2013.Log2FC", "RNAseqWholeSeedling.Log2FC", "RNAseqRoot.Log2FC")
```

Add a column indicating the direction of the expression change in response to cytokinin. We have `r length(indStuds)` indipendent primary data sets to consider for wild type response to BA:  

 * the wild types in the 2008 micro array study by Argyros et al.
 * the 2013 micro arry study by the Schaller lab
 * the RNA-Seq of whole seedlings by Apurva Bhargava
 * the RNA-Seq with separate roots and shoots by Tracy Rains (we'll use only the root)
 
 
```{r}
dfTest=df[,indStuds]
# recall that for the Sch2008 study, we kept genes that were in any of several categories.  Here, we only want to consider the ones where the wild type change was significant, (ie, a fold change of at least 2)
dfTest$Sch2008.Log2FC[abs(dfTest$Sch2008.Log2FC)<1] = NA
#
dfTest$ups=apply(dfTest, 1, function(v){sum(v > 0, na.rm=T)})
dfTest$downs=apply(dfTest, 1, function(v){sum(v < 0, na.rm=T)})
#
w=which(dfTest$ups>0)
dfTest[w,"direction"]=dfTest$ups[w]
w=which(dfTest$downs>0)
dfTest$direction[w]=-dfTest$downs[w]
w=which(dfTest$ups>0 & dfTest$downs>0)
dfTest$direction[w] = 0
#
df$direction=dfTest$direction
```

Look at this in terms of individual (non-redundant) genes.
```{r}
# drop any genes that did not appear in any of the results from previous comparisons
dfGenes=df[df$geneName %in% genesToKeep,]
# Remove duplicate entries for the same gene
dfGenes=dfGenes[!duplicated(dfGenes$geneName),]
```
```{r echo=F}
table(dfGenes$direction)
```

Of the `r sum(!is.na(dfGenes$direction))` genes with peaks that were also DE in at least one study:

 * `r sum(dfGenes$direction == 0, na.rm=T)` were DE in more that one study but not the same direciton,
 * `r sum(dfGenes$direction > 0, na.rm=T)` were up-regulated, and
 * `r sum(dfGenes$direction < 0, na.rm=T)` were down-regulated.


Show up-regulated genes by study.

```{r echo=F, fig.width=5, fig.height=3}
in3orMore=function(vennOut){
  vennOut=sapply(attr(vennOut,"intersections"), length)
  sum(vennOut[c("0111","1011","1101","1110","1111")], na.rm=T)
}
par(mar=c(0,1,0,1))
w = which(dfGenes$direction > 0)
vpos = with(dfGenes, venn(list(
  ma2008=geneName[intersect(w, which(Sch2008.Log2FC > 0))],
  ma2013=geneName[intersect(w, which(Sch2013.Log2FC > 0))],
  whole=geneName[intersect(w, which(RNAseqWholeSeedling.Log2FC > 0))],
  root=geneName[intersect(w, which(RNAseqRoot.Log2FC > 0))])))
```

Of the `r sum(dfGenes$direction > 0, na.rm=T)` up-regulated genes, `r in3orMore(vpos)` were up-regulated in at least 3 of 4 studies.

Show down-regulated genes by study.

```{r echo=F, fig.width=5, fig.height=3}
par(mar=c(0,1,0,1))
w = which(dfGenes$direction < 0)
vneg = with(dfGenes, venn(list(
  ma2008=geneName[intersect(w, which(Sch2008.Log2FC < 0))],
  ma2013=geneName[intersect(w, which(Sch2013.Log2FC < 0))],
  whole=geneName[intersect(w, which(RNAseqWholeSeedling.Log2FC < 0))],
  root=geneName[intersect(w, which(RNAseqRoot.Log2FC < 0))])))
```

Of the `r sum(dfGenes$direction < 0, na.rm=T)` down-regulated genes, `r in3orMore(vneg)` were down-regulated in at least 3 of 4 studies.

Show genes with alternative directions by study.

```{r echo=F, fig.width=5, fig.height=3}
par(mar=c(0,1,0,1))
w = which(dfGenes$direction == 0)
with(dfGenes, venn(list(
  ma2008=geneName[intersect(w, which(!is.na(Sch2008.Log2FC)))],
  ma2013=geneName[intersect(w, which(!is.na(Sch2013.Log2FC)))],
  whole=geneName[intersect(w, which(!is.na(RNAseqWholeSeedling.Log2FC)))],
  root=geneName[intersect(w, which(!is.na(RNAseqRoot.Log2FC)))])))
```


The column Sch2008.FC.WildTypeToMutantRatio describes how much greater the fold change was in the wild type compared to the fold change in the mutant--so we can think of this as the affect of having intact type-B RRs.

```{r fig.height=3, fig.width=4}
par(mar=c(0,0,0,0))
venRes=venn(list(TBup=which(dfGenes$Sch2008.FC.WildTypeToMutantRatio > 0),
          TBdown=which(dfGenes$Sch2008.FC.WildTypeToMutantRatio < 0),
          ExpUp=which(dfGenes$direction > 0), 
          ExpDown=which(dfGenes$direction < 0)))
```
```{r echo=F}
venRes=venRes[,"num"]
```

"TB" indicates the effect of Type Bs based on the 2008 ratio of fold changes. "Exp" indicates the direction of cytokinin regulation based on the expression data.

 * `r venRes["1010"]` genes were up-regulated in response to cytokinin in at least one dataset *and* were *more* induced in the wild type (type-B present) than in the mutant (type-B null).
 * `r venRes["0101"]` genes were down-regualated in the expression data *and* were *more* represed in the wild type compared to the mutant.
 * Only `r sum(venRes[c("0110","1001")])` genes had values for both types of data but did not fit into one of these cases.
 
In short, whatever affect we see in the wild type, we see less of it in the mutant.  For `r sum(venRes[c("1000","0100")])` genes we only have the 2008 wild type to mutant ratio; these are probably genes that were on the list because of a difference between the genotypes in one or both conditions, not because of any difference in the wild type between conditions.  For `r sum(venRes[c("0010","0001")])` genes, we have regulation from some data set but we don't have a value for "TBup" or "TBdown" because the gene was not sufficiently detected in all four sample types in the 2008 study.


We expect that the golden list will be a good indicator of genes being up- or down-regulated in our expression data.
```{r fig.height=3, fig.width=4}
par(mar=c(0,0,0,0))
venn(list(GLup=which(dfGenes$GoldenList>0),
          GLdown=which(dfGenes$GoldenList<0),
          ExpUp=which(dfGenes$direction>0), 
          ExpDown=which(dfGenes$direction<0)))
```

"GL" indicates the golden list (2013 meta analysis). "Exp" indicates the direction of cytokinin regulation based on the expression data.

We see almost complete agreement between the direction predicted by the golden list and the direction of regulation in these expression sets. No surprise there.


## How far are peaks from the TSS of a DE gene?
```{r }
par(mfrow=c(1,3))
source("../src/SigOverlap.R")
addLineForCrossingTSS=function(){
  abline(h=sum(dfTemp$fromTSS<0), xpd=T, col="pink")
}
#
dfTemp=df[which(df$direction>0),]
plotTSSdots(dfTemp, main="induced by BA")
addLineForCrossingTSS()
#
dfTemp=df[which(df$direction<0),]
plotTSSdots(dfTemp, main="repressed by BA")
addLineForCrossingTSS()
#
dfTemp=pg
plotTSSdots(dfTemp, main="all genes and peaks")
addLineForCrossingTSS()
```

In the above plot, the colored points indicate that the gene or the peak is duplicated, meaning it is associated with another gene/peak, one of the relationships might be just noise. A negative value on the x-axis indicates that the peak is upstream of the gene. The pink horizontal line highlights where the line of data points crosses over the TSS in each panel.

Neither the up- nor down-regulated groups look very different from the whole set. Both are within the range we might expect for a random sampling of elements from all the peak/gene associations, as shown below.

```{r fig.height=4, fig.width=5, echo=F}
N=1000
percents=sapply(X=1:N,FUN=test.per, 
                fullPool=row.names(pg), 
                areAlso=row.names(pg)[pg$fromTSS<0], 
                sampleSize=300)
realPer=sum(dfTemp$fromTSS<0)/nrow(pg)*100
plotLikelihood(randomPercents=percents, 
               realPercent=realPer,
               main="percent upstream of gene")
text(x=realPer, y=par("usr")[4], label="all", col="red", xpd=T, pos=3)
# up
upLine=sum(df[which(df$direction>0),]$fromTSS<0)/length(which(df$direction>0))*100
abline(v=upLine, col='red')
text(x=upLine, y=par("usr")[4], label="up", col="red", xpd=T, pos=3)
# down
downLine=sum(df[which(df$direction<0),]$fromTSS<0)/length(which(df$direction<0))*100
abline(v=downLine, col='red')
text(x=downLine, y=par("usr")[4], label="down", col="red", xpd=T, pos=3)
```


 

# Write results files

Put the rows of the main data frame in the same order as the rows in the heat map.
```{r}
# this number is used a lot
l=length(row.names(dfEx))
# give each gene a number based on its position in the heatmap
geneOrder=1:l
names(geneOrder)=row.names(dfEx)
# Give all other genes a slightly higher number
otherGenes=df$geneName[df$geneName %in% row.names(dfEx)]
geneOrder[l+1:l+length(otherGenes)]=l+1
names(geneOrder)[l+1:l+length(otherGenes)]=otherGenes
# reorder this list to match the gene names in df
geneOrder=geneOrder[df$geneName]
# reorder df using the arrangement that would put geneOrder in order
df=df[order(geneOrder),]
```

```{r echo=F}
totalGenes=length(unique(genesToKeep))
PerGenes=100*totalGenes/33603 #33603 unique genes in the TAIR10.bed.gz file

supportedGenesFromList=length(unique(intersect(genesToKeep, pg$geneName)))
PerGenesFromPeakList=100*supportedGenesFromList/length(unique(pg$geneName))

totalPeaks=length(unique(df[df$geneName %in% genesToKeep,]$peakName))
PerPeaks=100*totalPeaks/(length(unique(pg$peakName)))
```

 * We had some relevant expression result for `r totalGenes` genes (`r round(PerGenes,1)`% of the genome).
 * `r supportedGenesFromList` of these genes are from the peaks-to-genes list (`r round(PerGenesFromPeakList,1)`% of genes from the peaks-to-genes list).             
 * These genes account for `r totalPeaks` peaks (`r round(PerPeaks,1)`% of peaks).

## Write peak list
Write the expression results for all peaks near genes that showed any change in expression due to cytokinin.
```{r}
dfPeaks=df[df$geneName %in% genesToKeep,]
fnamePeaks="results/DEgenePeaksList.txt"
write.table(dfPeaks, file=fnamePeaks, sep='\t', row.names=F, quote=F)
system(paste("gzip -f", fnamePeaks))
```
This set includes

 * `r nrow(dfPeaks)` rows.
 * `r length(unique(dfPeaks$geneName))` unique genes.
 * `r length(unique(dfPeaks$peakName))` unique peaks.

## non-redundant gene list with gene expression info

```{r}
# drop any genes that did not appear in any of the results from previous comparisons
dfGenes=df[df$geneName %in% genesToKeep,]
# drop the peak info columns
dfGenes=dfGenes[,c(1,13:22)]     #dfGenes[,c(1,7,18,9:17,8)]
# Remove duplicate entries for the same gene
dfGenes=dfGenes[!duplicated(dfGenes$geneName),]
# Save to a file, and zip the file to save space.
fnameSet="results/GeneList.txt"
write.table(dfGenes, file=fnameSet, sep='\t', row.names=F, quote=F)
system(paste("gzip -f", fnameSet))
```
The saved file includes `r nrow(dfGenes)` rows and `r length(unique(dfGenes$geneName))` unique genes.



# Conclusions

**How many genes were in more than one expression set?  Among the genes that had peaks, were the same genes up or down regulated in the different data sets we've considered?**
Many genes only have support from one ore two data sets.  As a general trend, the direction of change is generally the same accross datasets, but this is not a hard rule.

```{r echo=F}
t=table(dfGenes$direction)
t
```
There were `r t['1']` genes that were up-regulated according to 1 data set, etc. `r in3orMore(vpos)` were supported by 3 or more.   
There were `r t['-1']` genes that were down-regulated according to 1 data set, etc.   `r in3orMore(vneg)` were supported by 3 or more. 
"0" indicates genes that were regulated in multiple data sets but in different directions.

Genes that were in more than one expression set were the most likely to also be near peaks.

**How far are peaks from the TSS of a DE gene? Does direction of regulation make a difference?**

Peaks are more often upstream of the gene than internal to it.The range immediately before the TSS is slightly more likely than the range slightly further away.  Both the up- and down-regulated groups are similar to the full set of all genes/peaks.  There is no correlation between distance from the TSS and the direction of regulation.

**SessionInfo**
```{r}
sessionInfo()
```

