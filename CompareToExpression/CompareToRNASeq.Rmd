---
title: "Are genes near peaks regulated by BA?"
output:
  html_document:
    toc: true
---

# Introduction

In this markdown, we compare genes that show ARR10 binding to genes that were regulated by cytokinin.  For genes with ARR10 binding, we will use the genes that were associated with the BApeaks. For genes regulated by cytokinin, we will use the differentially expressed genes from the RNA-seq experiment done by Bhargava et al. 2013.

**Questions**:

* Are DE genes more likely to be near peaks than randomly chosen or non-DE genes?
* Are genes near peaks, more lilely to be DE than genes that are not near peaks?
* Among the genes are are near a peak and DE, are they typically up- or down-regulated?
* If a peak is upstream of a gene on each strand, how often are both genes DE?

# Analysis

## Link ChIP-Seq and RNA-Seq data

### Read peak to gene assignment file

```{r}
fname="../GeneAssignment/results/ConsensusPeaks_ClosestGene_limit1500bp.txt" 
pg=read.delim(fname,header=T,sep='\t',as.is=T)
original.pg=pg # make a copy
```

### Read RNA-Seq results

The RNA-Seq results contain 

* normalized expression (in RPKM) values for treatment and control samples
* FDR and log2 fold-change indicating whether the gene was differentially expressed and in which direction

```{r}
# from https://bitbucket.org/lorainelab/atcytokininrnaseq
fname='../ExternalDataSets/Cytokinin.tsv.gz'
rnaseq=read.delim(fname,sep='\t',header=T,quote='',as.is=T)
rnaseq=rnaseq[,c("locus", "logFC", "fdr", "cn.ave", "tr.ave")]
names(rnaseq)[1]="gene"
num_expressed=nrow(rnaseq)
percent_expressed=round(num_expressed/33602*100,1)
```

There were `r nrow(rnaseq)` genes with at least one RNA-Seq read in the BA-treated or mock-control samples. There are 33,602 annotated genes in the Arabidopsis genome. This means that the RNA-Seq experiment detected expression of around `r percent_expressed`% of genes. 

### Merge RNA-Seq and ChIP-Seq

Merge the data frames:

```{r}
m=merge(rnaseq,pg,by.x='gene', by.y='geneName')
```

Only genes that had an entry in both ChIP-Seq and RNA-Seq data sets will appear in the merged data set. Around `r percent_expressed`% of all genes were in the RNA-Seq data set. So if the genes in the ChIP-Seq data set were just randomly selected, then we'd expect about `r percent_expressed` of them to also be in the RNA-Seq data set. 

```{r}
percent_in_both=round(length(unique(m$gene))/length(unique(pg$geneName))*100,1)
```

Instead, `r percent_in_both`% of them have an entry in the RNA-Seq data set. 

## Random sampling method

Use random sampling to demonstrate that this is (or is not) very unlikley to happen by random chance.

```{r fig.width=7,fig.height=6}
# get all the genes in the genome
# from https://bitbucket.org/lorainelab/arabidopsis-annotation-update
fname='../ExternalDataSets/genedescr.tsv.gz'
genes=read.delim(fname,header=T,sep='\t',as.is=T,quote='')
set.seed(23)
source("../src/SigOverlap.R")
test.per
N=1000
# If I randomly select 'sampleSize' elements from the entire group of 'fullPool',
# what percentatge of them are also elements of 'areAlso'?

# If I did this random test 'N' times, what sort of distribution would I get?
plotLikelihood

percents=sapply(X=1:N,FUN=test.per, 
                fullPool=genes$AGI, 
                areAlso=rnaseq$gene, 
                sampleSize=length(unique(pg$geneName)))

plotLikelihood(randomPercents=percents, realPercent=percent_in_both,
               main="Annotated Genes that are Expressed")
text(x=percent_in_both, y=par("usr")[4], label="genes near peaks", col="red", xpd=T, pos=2)
```

The average percent from `r N` random samples was `r mean(percents)` and the maximum percent was `r max(percents)`. Thus, even though though we sampled 1000 times, we didn't once observe a value as large as the observed value of `r percent_in_both`. 

This demonstrates that there is a non-random relationship between genes that have at least one read in the RNA-Seq data, and genes that are near a peak in the ChIP-Seq data.  It may be something as simple as "these genes are real" or these genes represent the open chromatin in these conditions.

## Differentially expressed genes

```{r}
source("../src/ProjectVariables.R")
FDR=RNAseqFDR() # see projectVariables
mDE=m[m$fdr<=FDR,]
num_DE=nrow(rnaseq[rnaseq$fdr<=FDR,])
per_DE_nearPeak=round(100*length(unique(mDE$gene))/num_DE,1)
```
Based on a false discovery rate of `r FDR`, the RNA-Seq study included `r num_DE` differentially expressed (DE) genes (`r round(num_DE/num_expressed*100, 1)`% of expressed genes).

Of all the BA peaks in the ChIP-Seq data

 * `r length(unique(pg$peakName))` of the BApeaks were assigned to at least one gene, (`r length(unique(pg$geneName))` genes)
 * `r length(unique(m$peakName))` were assigned to at least one expressed gene, (`r length(unique(m$gene))` genes) and 
 * `r length(unique(mDE$peakName))` of them were assigned to at least one DE gene (`r length(unique(mDE$gene))` genes).
 * This means `r per_DE_nearPeak`% of DE genes were near a peak.

```{r}
mNonDE=m[m$fdr>FDR,]
num_NonDE=nrow(rnaseq[rnaseq$fdr>FDR,])
per_nonDE_nearPeak=round(100*length(unique(mNonDE$gene))/num_NonDE,1)
```
* `r per_nonDE_nearPeak`% of non-DE genes were near a peak.

### Are DE genes more likely to be near peaks?

```{r}
percentsNonDE=sapply(1:N,test.per, 
                     fullPool=rnaseq$gene, 
                     areAlso=unique(pg$geneName), 
                     sampleSize=num_NonDE)
plotLikelihood(randomPercents=percentsNonDE, realPercent=per_nonDE_nearPeak,
               main="Expressed Genes that are Near Peaks")
text(x=per_nonDE_nearPeak, y=par("usr")[4], label="nonDE", col="red", xpd=T, pos=2)
```

 * The number of non-DE genes is a very large number, so it gets very consistent results.
 * Non-DE genes are less likley (compared to random chance) to be genes near peaks. 
 * The differnce in liklelihood is only about `r round(mean(percentsNonDE)-per_nonDE_nearPeak, 1)`%.

```{r}
percentsDE=sapply(1:N,test.per, 
                  fullPool=rnaseq$gene, 
                  areAlso=unique(pg$geneName), 
                  sampleSize=num_DE)
plotLikelihood(randomPercents=percentsDE, realPercent=per_DE_nearPeak,
               main="Expressed Genes that are Near Peaks")
text(x=per_DE_nearPeak, y=par("usr")[4], label="DE genes", col="red", xpd=T, pos=2)
```

 * The number of DE genes is a much smaller number, so the distribution is wider, but the mean is similar (`r mean(percentsNonDE)` compared to `r mean(percentsDE)`)
 * DE genes are more likley (compared to random chance) to be genes near peaks. 
 * The differnce in liklelihood is about `r round(per_DE_nearPeak-mean(percentsDE), 1)`, which is about `r round((per_DE_nearPeak-mean(percentsDE))/sd(percentsDE),0)` standard deviations from the mean of the random selections.

### Are genes near peaks, more lilely to be DE than genes that are not near peaks?

```{r}
num_nearPeak <- length(unique(pg$geneName))
num_DEnearPeak <- length(unique(mDE$gene))
perPGthatAreDE <- round(100*num_DEnearPeak/num_nearPeak,1)
```

 * `r num_nearPeak` genes were near a BApeak.  
 * `r num_DEnearPeak` of those were DE (`r perPGthatAreDE`)%.
 
Is that unusually high or low compared to what we would see by random chance?

```{r}
percentsNearPeak=sapply(1:N,test.per, 
                     fullPool=rnaseq$gene, 
                     areAlso=rnaseq[rnaseq$fdr<=FDR,"gene"], 
                     sampleSize=num_nearPeak)
plotLikelihood(randomPercents=percentsNearPeak, realPercent=perPGthatAreDE,
               main="Expressed Genes that are Differentially Expressed")
text(x=perPGthatAreDE, y=par("usr")[4], label="gene near peaks", col="red", xpd=T, pos=2)
```

 * Genes near peaks are more likley (compared to random chance) to be genes near peaks. 
 * The differnce in liklelihood is about `r round(perPGthatAreDE-mean(percentsNearPeak), 1)`, which is about `r round((perPGthatAreDE-mean(percentsNearPeak))/sd(percentsNearPeak),0)` standard deviations from the mean of the random selections.
 
Genes near a peak are more likley than random to be DE, but the likelihood is only `r perPGthatAreDE`, so this is not a good stand-alone indicator of genes that will be DE.

### Among the genes that are near a peak _and_ DE, are they typically up- or down-regulated?

```{r}
nearPeak=rnaseq$gene %in% pg$geneName
isDE=rnaseq$fdr<=FDR

DEnearPeak <- rnaseq[nearPeak & isDE,"logFC"] # which genes are near peaks AND are DE
DEnearPeak = ifelse(DEnearPeak>0, yes="Up", no="Down")
tDE=table(DEnearPeak)

DEnoPeak <- rnaseq[isDE & !nearPeak,"logFC"]
DEnoPeak = ifelse(DEnoPeak>0, yes="Up", no="Down")
tDEnoPeak <- table(DEnoPeak)
```

```{r echo=FALSE}
par(mfcol=c(1,2))
# DE genes near peaks
bp=barplot(tDE, col=c("pink", "lightblue"), main="DE genes near peaks", axes=F)
abline(h=0)
text(x=bp, y=tDE, labels=tDE, xpd=T, pos=3)
# all DE genes
bp=barplot(tDEnoPeak, col=c(gray(.3), gray(.7)), main="DE genes witout peaks", axes=F)
abline(h=0)
text(x=bp, y=tDEnoPeak, labels=tDEnoPeak, xpd=T, pos=3)
```

 * `r round(100*tDE["Up"]/length(DEnearPeak),1)`% of DE genes near peaks are up-regulated.
 * `r round(100*tDEnoPeak["Up"]/length(DEnoPeak),1)`% of all the other DE genes are up-regulated.



### Are multiple genes regulated through one peak?
Recall that each BApeak was assigned to the nearest downstream gene in each direction as well as any gene it was internal.  
```{r}
t=table(table(pg$peakName))
t
```

Among peaks associated with genes:

 * There were `r t[1]` peaks with only one gene within our maximum distance.
 * There were `r t[2]` peaks assigned to two genes.
 * There were `r t[3]` peaks which must be internal to a gene and have a gene nearby in both directions.
 * And `r sum(t[4:length(t)])` peaks that are internal to more than one gene.

```{r}
tDEpeaksPerGene=table(table(mDE$peakName))
tDEpeaksPerGene
```

Among peaks associated with DE genes:

 * There are `r tDEpeaksPerGene[1]` peaks that are linked to exactly one DE gene.
 * There are `r sum(tDEpeaksPerGene[2:length(tDEpeaksPerGene)])` peaks that are linked to more than one DE gene.
 
Exp1_Rep2_peak_4865,Exp1_Rep1_peak_4765,Exp2_BA_peak_4157 is internal to AT2G45680 and near the TSS for AT2G45685.

Exp1_Rep1_peak_4766,Exp1_Rep2_peak_4866,Exp2_BA_peak_4158 is internal to AT2G45685 and near the TSS for AT2G45680.

Conversely, do many DE genes have multiple peaks?
```{r}
tMultiPeaks=table(table(mDE$gene))
tMultiPeaks
```

```{r}
temp=table(mDE$gene)
MultiplePeaks=temp[temp>1]
MultiplePeaks
```

We should take a look at these in IGB.

### How far are peaks from the TSS of a DE gene?

```{r}
# mDE=mDE[order(mDE$fromTSS),]
# isDup=duplicated(mDE$peakName) | duplicated(mDE$gene)
# col=rep(1, nrow(mDE))
# col[isDup]=19
# plot(x=mDE$fromTSS, y=1:length(mDE$fromTSS),
#      main="Distance from peak to TSS",
#      ylab="ID", xlab="distance (bp)", col=col, pch=col, las=1)
# abline(v=0, col=gray(.2))
# text(x=0, y=50, labels="Transcription Start Site", pos=4, col=gray(.2))
# 
plotTSSdots(mDE)
```

In the above plot, the colored points indicate that the gene or the peak is duplicated, meaning it is associated with another gene/peak, one of the relationships is just noise. A negative value on the x-axis indicates that the peak is upstream of the gene.

# Write results to a file

Write a file that includes all genes that were related to a peak.
```{r}
fnameAll="results/AllGenesExpressionToBinding.txt"
write.table(m[order(m$gene),], file=fnameAll, sep='\t', row.names=F, quote=F)
system(paste("gzip -f", fnameAll))
```
This file, `r fnameAll` includes 

 * `r nrow(m)` rows
 * `r length(unique(m$gene))` genes and 
 * `r length(unique(m$peakName))` peaks.
Rows are ordered by gene, so they will be easy to follow along in IGB.
 
Write a file that includes only the DE genes that were related to a peak.
```{r}
fnameDE=paste0("results/DEgenesExpressionToBinding_fdr",FDR,".txt")
write.table(mDE[order(mDE$gene),], file=fnameDE, sep='\t', row.names=F, quote=F)
system(paste("gzip -f", fnameDE))
```
This file, `r fnameDE` includes 

 * `r nrow(mDE)` rows
 * `r length(unique(mDE$gene))` genes and 
 * `r length(unique(mDE$peakName))` peaks.


# Conclusions

**Are DE genes more likely to be near peaks than randomly chosen or non-DE genes?**  

Yes, DE genes are near peaks `r per_DE_nearPeak`% of the time, but only about `r mean(percentsDE)` would expected by random chance.

**Are genes near peaks, more lilely to be DE than genes that are not near peaks?**   

Genes near a peak are more likley than random to be DE, but the likelihood is only `r perPGthatAreDE`, so this is not a good stand-alone indicator of genes that will be DE.

**Among the genes are are near a peak and DE, are they typically up- or down-regulated?**

`r round(100*tDE["Up"]/length(DEnearPeak),1)`% of DE genes near peaks are up-regulated.
vs `r round(100*tDEnoPeak["Up"]/length(DEnoPeak),1)`% among all the other DE genes.

**If a peak is upstream of a gene on each strand, how often are both genes DE?**

There are `r sum(t[2:length(tDE)])` instances of a peak being linked to more than one DE gene.

**SessionInfo**
```{r}
sessionInfo()
plotTSSdots
```

