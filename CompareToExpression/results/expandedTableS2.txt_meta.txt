Supplemental Table S2: Target genes of ARR10.  804 genes associated with 882 peaks.  900 peak-gene relationships

Sources for expression information:
RNAseqWholeSeedling(2013): GenBank Bioproject PRJNA194426
RNASeqRoot and RNASeqShoot (this study): Short Read Project SRP059384
Argyros2008: E-MEXP-1573
Sch2013: E-MTAB-1596
GoldenList(2013): Bhargava et al, 2013 Plant Phys 162:272-294

Peak colunn definitions:
peak1 is the closest peak to the transcription start site (TSS)
If additional peaks were associated with a gene, they were called peak2, peak3
peakName - the peak id in each of the BA-treated samples
summit - genomic location of the peak summit
fromTSS - genomic distance between the TSS and the peak (TSS position - summit position)
BAfactor - the fold enrichment in the BA-treated sample / fold enrichment in the mock sample, where fold enrichment is derived from MACS2 bgcmp for each sample
BAfactorLimitedToMockPeaks - same as BAfactor, but fold enrichment is taken from peaks called by MACS. Where there is no peak in the mock sample there is an NA value.
