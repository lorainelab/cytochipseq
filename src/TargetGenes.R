# Target Genes

# function(s) used in identifying the most likely target genes in a chip seq experiment


# Using the output from geneAssign, calculate an overall score for each gene,
# using the summary (iether the sum or the maximum) of the fold changes for all of the surrounding peaks.
# value: a data frame, with one row for each gene, and columns as follows:
#  gene: each unique element from the input gene column.
#  allsum: gene score, depends on keepScore argument
#  u3: summary of scores of peaks more than 2kb upstream from gene start
#  u2: summary of scores of peaks 1kb - 2kb upstream from gene start
#  u1: summary of scores of peaks 0-1kb upstream from gene start
#  iG: summary of scores of peaks in the region of the gene
#  d1: summary of scores of peaks 0-1kb downstream from gene end
geneEnrichment <- function(peakgenes, scoreCol="foldEnrichment", keepScore="sum", negLogPValCol=NULL, negLogQValCol=NULL){
  #peakgenes: output of geneAssign, should include geneName, peakName, location of peak summit relative to gene start, location relative to gene end, and a score column
  #scoreCol: character string. column of scores to use
  #keepScore: character string. for genes with multiple peaks, should the final score be the "sum" or the maximim "max" of all peaks scores. 
  #  If covering several regions around a gene, consider "maxSum", to get the sum for each region, and take the single greatest region value.
  #negLogPValCol: a character string. column of negative Log 10 p values for each peak in pairing.
  #negLogQValCol: a character string. column of negative Log 10 q values for each peak in pairing. The Q values should be the adjusted pvalues.
  #
  #define the main function used to make each new row by using either the sum or max method.
  if (keepScore=="sum"){
    dothis<-function(df,pg){
      gene=as.character(pg$geneName[1])
      u3=sum(as.numeric(pg[pg$fromTSS<=-2000,scoreCol]))
      u2=sum(as.numeric(pg[(pg$fromTSS>-2000 & pg$fromTSS<=-1000),scoreCol]))
      u1=sum(as.numeric(pg[(pg$fromTSS>-1000 & pg$fromTSS<=0),scoreCol]))
      iG=sum(as.numeric(pg[(pg$fromTSS>0 & pg$fromTES<=0),scoreCol]))
      d1=sum(as.numeric(pg[(pg$fromTES>=0),scoreCol]))
      allsum=sum(as.numeric(pg[,scoreCol]))
      newRow=c(gene,allsum,u3,u2,u1,iG,d1)
      newdf=rbind(df,newRow)
      return(newdf)
    }
  }
  if (keepScore=="max"){
    dothis<-function(df,pg){
      gene=as.character(pg$geneName[1])
      u3=max(0, pg[pg$fromTSS<=-2000,scoreCol])
      u2=max(0, pg[(pg$fromTSS>-2000 & pg$fromTSS<=-1000),scoreCol])
      u1=max(0, pg[(pg$fromTSS>-1000 & pg$fromTSS<=0),scoreCol])
      iG=max(0, pg[(pg$fromTSS>0 & pg$fromTES<=0),scoreCol])
      d1=max(0, pg[(pg$fromTES>0),scoreCol])
      allmax=max(0, pg[,scoreCol])
      newRow=c(gene,allmax,u3,u2,u1,iG,d1)
      newdf=rbind(df,newRow)
      return(newdf)
    }
  }
  if (keepScore=="maxSum"){
    dothis<-function(df,pg){
      gene=as.character(pg$geneName[1])
      u3=sum(as.numeric(pg[pg$fromTSS<=-2000,scoreCol]))
      u2=sum(as.numeric(pg[(pg$fromTSS>-2000 & pg$fromTSS<=-1000),scoreCol]))
      u1=sum(as.numeric(pg[(pg$fromTSS>-1000 & pg$fromTSS<=0),scoreCol]))
      iG=sum(as.numeric(pg[(pg$fromTSS>0 & pg$fromTES<=0),scoreCol]))
      d1=sum(as.numeric(pg[(pg$fromTES>=0),scoreCol]))
      allsum=max(u3,u2,u1,iG,d1)
      newRow=c(gene,allsum,u3,u2,u1,iG,d1)
      newdf=rbind(df,newRow)
      return(newdf)
    }
  }
  #
  #Create a data frame around a starter row
  df=data.frame(gene="geneName", Grand=20, u3k=0, u2k=0, u1k=0, inGene=0, d1k=0, stringsAsFactors=F)
  if (keepScore=="sum"){names(df)[2]=paste0("Sum.All.",scoreCol)}
  if (keepScore=="max"){names(df)[2]=paste0("Max.",scoreCol)}
  #
  peakgenes$geneName=as.factor(as.character(peakgenes$geneName))
  peakgenes$fromTSS=as.numeric(as.character(peakgenes$fromTSS))
  peakgenes$fromTES=as.numeric(as.character(peakgenes$fromTES))
  #
  for (I in levels(peakgenes$geneName)){
    pg <- peakgenes[peakgenes$geneName==I,] #peakgenes mini (pg)
    df=dothis(df, pg)
  }
  #
  #account for starter row of df
  df=df[2:nrow(df),]
  #
  #look for lowest p and q values
  if (!is.null(negLogPValCol)){
    bestPval=c()#vector to hold the maximum negLog10pVal for each gene
    for (I in levels(as.factor(df$gene))){
      pg <- peakgenes[peakgenes$geneName==I,] #peakgenes mini (pg)
      m=max(pg[,negLogPValCol])
      bestPval=c(bestPval,m)
    }
    df=cbind(df,bestPval)
  }
  if (!is.null(negLogQValCol)){
    bestQval=c()#vector to hold the maximum negLog10pVal for each gene
    for (I in levels(as.factor(df$gene))){
      pg <- peakgenes[peakgenes$geneName==I,] #peakgenes mini (pg)
      m=max(pg[,negLogQValCol])
      bestQval=c(bestQval,m)
    }
    df=cbind(df,bestQval)
  }
  #ensure that numeric data is treated as numeric so as to avoid problems in downstream functions.
  for(i in 2:ncol(df)){
    df[,i]=as.numeric(df[,i])
  }
  return(df)
} # end geneEnrichment function


# look at a subset of genes from a larger data frame
# previously called makeCheck()
checkSubset <- function(genes, checkGenes, name, rankby=name){
  # genes: data frame with gene identifiers in column [name]
  # checkGenes: a named vector of genes to find in genes, with the ID as the names of elements matching the ids in [name]
  # name: character string indicating the column of gene identifiers.
  # rankby: character string indicating which column should be used to sort the output
  #
  df <- genes[(as.character(genes[,name]) %in% names(checkGenes)),] # subset the data frame to only include genes of interest
  df$symbol = checkGenes[as.character(df[,name])]
  df=df[order(df[,rankby], decreasing=T),]
  return(df)
}



# Determine an appropriate score threshold for a set of genes.  This function plots the scores as a histogram, and (optional) labels 
# genes of interest on the plot based on their scores.  It also returns a suggested value to use as the cut off, and  (optional) draws 
# that on the plot as well to use as a reference against the genes of interest.
# value: plot is drawn and a numeric value (recommended cuttoff) is returned.
# previously called makeCheckPlot()
threshPickerPlot <- function(genes, checkGenes=NULL, name=names(genes)[1], column=names(genes)[2], 
                          main=paste("Histogram of Scores in", column), 
                          mainColor="grey", breaks=seq(0,max(genes[,column])+5,5), 
                          plotChecks=F, recommend.cutoff=F, prob=.05, 
                          useTrueMean=T, dropZeros=T, normCol="darkblue", scoreCutOff=NULL, append=F)
{
  # genes: output of geneEnrichment() (or genesWithPeaks {CSAR} or distance2Genes(){CSAR})
  #   data frame with gene identifiers in column [name]
  # checkGenes: a named vector of genes to find in genes, with the ID as the names of elements matching the ids in [name]
  # name: character string, column name of gene identifiers
  # column: character string indicating the name of the column for scores to be used for each gene
  # main: character string, main title to use in plot
  # mainColor: color to used for main line in plot
  # breaks: value to use for 'breaks' argument to histogram
  # plotChecks: logical, should the plot include markings for a subset of genes of interest. if true, checkGenes is required.
  # recommend.cutoff: logical, should the calculated cutoff based on [prob] be drawn on the plot (the cutoff is still calculated and returned).
  # prob: numeric between 0 and 1, used to calculate the a cuttoff. .05 calculates the 95th quantile.
  # useTrueMean: logical, if true, the cutoff is calculated based on the mean of the data. if false, the mode is used as a pseudo mean.
  # dropZeros: logical, should the rows with with 0-values in [column] be excluded
  # normCol: color to used for plotting normal distribution (if plotted) and the calculated cutoff (if plotted)
  # scoreCutOff: numeric, if you already have a cutoff in mind and you just want to plot that, enter it here.
  # append: logical, should the plot be added to an existing plot
  #
  # drop rows with 0 for a score (if dropZeros==T)
  if (dropZeros){genes=genes[genes[,column] > 0,]}
  #
  # make data frame with the subset of genes of interest to draw on plot
  Checker=checkSubset(genes, checkGenes, name=names(genes)[1])
  if (nrow(Checker)==0){plotChecks=F}
  #
  # calculate the number of occurances of difference scores
  histogramRecord <- hist(genes[,column], breaks=breaks, plot=F)
  #
  # prep plot area
  x=histogramRecord$mids
  y=histogramRecord$counts
  if (!append){ # set up plot area
  plot(x=x, y=y, 
       xlab="score", ylab="frequency", main=main,
       type="n")
  }
  # draw a curve of the histogram values
  lines(x, y, col=mainColor)
  w=which(histogramRecord$counts==0)
  for (i in w){
    lines(x=c(x[i],x[i]), y=c(0,-10), col=mainColor) # This shows which portions of the line are zero-counts. 
  }
  #
  # figure out the mean (either the mean of the data, or the mode based on the histogram to use as a pseudo mean)
  ymax=max(histogramRecord$counts)
  m=max(histogramRecord$breaks[which(histogramRecord$counts==ymax)+1])
  m2=mean(genes[,column])
  m2=round(m2, digit=2)
  if (useTrueMean) {m=m2}  
  #
  # figre out the standard deviation of the sample
  sd=sd(genes[,column])
  #  
  # calculate the cutoff to use to only get the top prob*100 percent of the genes. This can be done based on the actual data (useTrueMean=T) or 
  # on a hypthetica set of numbers where we assume that the non-significant numbers are normally distributed below the significant ones 
  # and we are choosing a cutoff to allow for [prob] of the non-significant genes to slip through. 
  set = rnorm(n=length(genes[,column]), mean=m, sd=sd)
  if (useTrueMean) {set=genes[,column]} 
  prob=1-prob
  a=quantile(set, probs=prob)
  a=round(a, digit=2)
  #
  # now we're done with calculations and we're adding things to the plot, so the ymax we care about is the plot ymax
  ymax=par("yaxp")[2]
  # draw the calculated cutoff (a) on the plot
  if (recommend.cutoff){
    lines(x=c(a,a), y=c(0,ymax), col=normCol)
    text(labels=a, x=a, y=ymax*.9, pos=4)
    if (useTrueMean) {print(paste0("Recommendation of threshold ", a, " based on taking the ", prob, "th quantile of the sample."))}
    else {print(paste0("Recommendation of threshold ", a, " based on taking the ", prob, "th quantile of the normal distribution with same sd as the sample and a mean of ", m, " which is really the mode. The real mean is ", m2,"."))
    }
  }
  # draw pre-entered cut off
  if (!is.null(scoreCutOff)){
    lines(x=c(scoreCutOff,scoreCutOff), y=c(0,ymax), col=normCol)
    text(labels=scoreCutOff, x=scoreCutOff, y=ymax, pos=4)
  }
  #
  #draw the genes of interest (checkGenes) on the plot for reference.
  if (plotChecks){
    ySet=seq(.2*ymax, .8*ymax, length.out=nrow(Checker))
    for (i in 1:nrow(Checker)){
      col = mainColor
      x=Checker[i,column]
      y=ySet[i]
      lines(x=c(x,x), y=c(0,y), col=col)
      text(labels=Checker$symbol[i], x=x, y=y, col=col, pos=3)
    }
  }
  #
  # return the calculated cutoff
  return(a)
}


