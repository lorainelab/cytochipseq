# MotifHunt.T

# use these functions for inspecting the occurances of motifs (regular expressions) in sets of sequences.
# Dependency: Biostrings package

# # function used as part of detectGroup.  It is used for each element of the list returned by gregexpr to extract the number of occurances.
# getCount=function(listItem){
#   # listItem: a vector that is the nth list item returned from gregexpr
#   l=length(which(listItem>0))
#   return(l)
# }

# detect which sequences have any of a group of motifs.
# value: a data frame giving the number of occurances of each motif in each reference sequence, 
#  and a column of T/F for each sequence indicating if (T) any of the motifs had any occurances.
# note: palendromes are counted twice. Overlapping occurances are treated as multiple occurances.
detectGroup <- function(referenceSet, motifSet){
  # referenceSet: object of type DNAStringSet conaining sequences to search in. could use a named vector of test strings.
  # motifSet: vector of iehter plain text or regular expressions which you want to find in referenceSet.
  df = data.frame(matrix(data=NA, nrow = length(referenceSet), ncol=length(motifSet)))
  names(df) = motifSet
  row.names(df) = names(referenceSet)
  for (I in motifSet){
    t1 = NULL
    t2 = NULL
    t1 = gregexpr(I, referenceSet, perl=T, ignore.case = T)
    t2 = gregexpr(I, reverseComplement(referenceSet), perl=T, ignore.case = T)
    t1 = sapply(t1, function(x){length(which(x>0))})
    t2 = sapply(t2, function(x){length(which(x>0))})
    counts = t1 + t2
    df[,I] = counts
  }
  df$anyMotif = apply(df[,motifSet], 1, function(x){any(x>0)})
  return(df)
}


# Determine the location of each of a set of motifs relative to the end of each of a set of sequences (such as promoter regions)
# value: a list of length 2 (starts and lengths), each element is a named list. The names correspond to the names of the sequences.
#   starts: the start of each motif from the set as it appears within the sequence
#   lengths: the length of the match.  If using planin sequence motifs (not regular expressions) these lengths will always represent the lenght of the motif.
locateGroup <- function(referenceSet, motifSet, printProgress=F, fixed=F){
  # referenceSet: object of type DNAStringSet conaining sequences to search in.
  # motifSet: vector of iehter plain text or regular expressions which you want to find in referenceSet.
  # printProgress: logical, should the program print messages to inform you of the progress
  # fixed: given to gregexpr; logical. If TRUE, pattern is a string to be matched as is.
  starts=as.list(rep(NA, length(referenceSet)))
  names(starts)=names(referenceSet)
  lengths=as.list(rep(NA, length(referenceSet)))
  names(lengths)=names(referenceSet)
  widths=referenceSet@ranges@width
  names(widths)=names(referenceSet)
  ret = list(starts=starts, lengths=lengths) #ret will ultimately be what is returned by the function
  for (I in names(referenceSet)){
    if (printProgress) {print(I)}
    refLength=widths[I] +1
    strs=NULL
    lens=NULL
    for (J in motifSet){
      t1 = NULL
      t2 = NULL
      t1 = gregexpr(J, referenceSet[I], perl=T, ignore.case = T, fixed=fixed)[[1]]
      if(t1[1]==-1){t1 = NULL} #gregexpr uses -1 to indicate that no match was found.
      t2 = gregexpr(J, reverseComplement(referenceSet[I]), perl=T, ignore.case = T, fixed=fixed)[[1]]
      if(t2[1]==-1){t2 = NULL}
      else {  # if t2 is not null, we want to look at the reverse comp matches in terms of the same coordinates set-up as the forward matches.
        t2=refLength - t2
        t2=t2-attributes(t2)$match.length +1
        } 
      #add each new group of starts and lengths to the starts and lengths for that sequence
      strs=c(strs, t1, t2)
      lens=c(lens, attributes(t1)$match.length, attributes(t2)$match.length)
    }#end motif for loop
    if (!is.null(strs)) {
      # put the starting positions of the various motifs into order by occurance
      ord = order(strs)
      strs=strs[ord]
      lens=lens[ord]
      #put this info into its place in ret
      ret[["starts"]][[I]]=strs
      ret[["lengths"]][[I]]=lens
    }
  }
  return(ret)
}



# Convert the ouput of locateGroup to a bed file
# value: a data frame with 3 columns in bed file format. 
ConvertToBed = function(GenomeMotifs){
  # GenomeMotifs: the output of locateGroup, a list of length 2, 
  #     with elements starts and lengths indicating positions of motif matches.
  bed=data.frame(matrix(data=NA,ncol=4,nrow=sum(sapply(GenomeMotifs$starts, length))))
  names(bed)=c("chromosome", "chromStart", "chromEnd", "name")
  
  stops=GenomeMotifs$starts #just to create an object of the right size
  for (J in names(GenomeMotifs$starts)){
    stops[[J]]=GenomeMotifs$starts[[J]]+GenomeMotifs$lengths[[J]]
  }
  
  chromosome = NULL
  chromStart = NULL
  chromEnd = NULL
  for (J in names(GenomeMotifs$starts)){
    chromosome=c(chromosome, rep(J, length(GenomeMotifs$starts[[J]])) )
    chromStart = c(chromStart, GenomeMotifs$starts[[J]])
    chromEnd = c(chromEnd, stops[[J]])
  }
  bed$chromosome=chromosome
  bed$chromStart=chromStart-1 #biostrings is 1-based, bed files are 0-based
  bed$chromEnd=chromEnd-1 #the end was defined useing the start
  
  #remove any where the entire chromosome had nothing
  w=which(!is.na(bed$chromStart))
  bed=bed[w,]
  return(bed)
}





# for an individual motif, get the total number of sequences (from a given reference set) that contain the motif.
detectRegExMotif <- function(motif, referenceSet) {
  t1 = grepl(motif, referenceSet, perl=T)
  #set up reverse complement
  t2 = grepl(motif, reverseComplement(referenceSet), perl=T)
  bool = t1 | t2
  ret = sum(bool)
  return(ret)
}

# for an individual motif, get a logical vecotor indicated which sequences (from a given reference set) contain the motif.
detectMotif2 <- function(motif, referenceSet) {
  t1 = grepl(motif, referenceSet, perl=T)
  #set up reverse complement
  t2 = grepl(motif, reverseComplement(referenceSet), perl=T)
  bool = t1 | t2
  return(bool)
}

#Randomly reorder the letters of a string.
shuffle.string <- function(string){
  # string 
  letters = unlist(strsplit(string, split=""))
  newString <- paste(sample(letters, length(letters), replace=FALSE),collapse="")
  return(newString)
}
