#!/usr/bin/env python

ex=\
"""
Make a table that reports peak scores by sample for super-summits. 
Reads
  - output from bedtools merge on summit regions.
  - combined peaks output from macs2
  
ex)

%prog -p PeakProcessing/results/peaks.tsv.gz -s PeakComparison/results/super_summits_10.bed

Writes to stdout
"""
import sys,optparse,gzip

# peaks file:
# abs_summit - field 4
# name - field 9
def main(peaks=None,super_summits=None):
    d = getPeaksDict(peaks=peaks)
    fh=openFile(super_summits)
    heads=['chr','start','end','name']
    samples=d.keys()
    sys.stdout.write('\t'.join(heads+samples)+'\n')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.rstrip().split('\t')
        subd={}
        name=toks[3]
        peak_names=name.split(',')
        for peak_name in peak_names:
            sample_name=peak_name.split('_peak')[0]
                # if subd.has_key(sample_name):
                # raise ValueError("Supersummit has >1 summit regions from the same peak %s"%name)
            subd[sample_name]=peak_name
        for sample in samples:
            if subd.has_key(sample):
                toks.append(d[sample][subd[sample]])
            else:
                toks.append('NA')
        sys.stdout.write('\t'.join(toks)+'\n')

def getPeaksDict(peaks=None):
    fh = openFile(peaks)
    fh.readline() # throw away header
    d = {}
    while 1:
        line=fh.readline()
        if not line:
            fh.close()
            break
        toks=line.rstrip().split('\t')
        name=toks[9]
        abs_summit=toks[4]
        sample=toks[-1]
        if not d.has_key(sample):
            d[sample]={}
        d[sample][name]=abs_summit
    return d

# open fname, can be .gz 
def openFile(fname):
    if fname.endswith('.gz'):
        fh=gzip.GzipFile(fname)
    else:
        fh=open(fname)
    return fh

if __name__ == '__main__':
    usage='%prog [options]\n'+ex
    parser=optparse.OptionParser(usage)
    parser.add_option('-p','--peaks_file',dest='peaks',
                      help='Peaks file with fold-change [required]')
    parser.add_option('-s','--super_summits',dest='super_summits',
                      help='Bedtools merge of summits into supersummits')
    (options,args)=parser.parse_args()
    if not options.peaks or not options.super_summits:
        parser.print_help()
    else:
        main(peaks=options.peaks,
             super_summits=options.super_summits)
