# Motif Enrichment #

This process has 4 major parts, each is a different mark down.  

* * *

## BindingAndNonBindingRegions.Rmd 
A short mark down that establishes the files used to represent the binding regions and trims all of the regions to a standard size. Then it selects background regions, and makes at least one file with the same size and number of regions as the binding sites file.  These are both stored as bed files.

* * *

## MoitfLocations.Rmd
This takes a few minutes to run.  It finds all occurrences of a set of motifs through the genome and stores the locations as a bed file.  The motifs it searches for are taken from a file in the data folder.

* * *

## MotifEnrichment-Figure.Rmd 
A shorter mark down whose only purpose is to make a figure for the paper.  It uses bed tools intersect to determine which binding and non-binding regions have motifs, and it makes a bar chart.

* * *

## MotifEnrichment-Exploritory
A longer mark down.  It has the same initial steps of using bed tools intersect to determine which regions have hits but rather than focusing on a single figure it makes many comparisons.

* * *

### data
The input data includes a table of the motifs.

* * *

### figure6E
The image(s) that were made at high enough resultions with proper labels inteded to be used as a figure in the paper.

* * *

### interemediateFiles
This directory is not stored in the repository.  Markdowns that write to it include a command to create this directory in case it does not already exist in your local directory.  Files describing the locations of motifs are stored here because they are not the final product of the analysis and they can be generated on your local machine.

* * *

### results
The final output includes the bed files showing motif locations.
