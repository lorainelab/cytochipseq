---
title: "Motif Enrichment - Exporitory"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction

## Questions
Is there any enrichment or relationship between any of these motifs?



# Motifs
From the primary motif AGATWCG; from secondary motif: AAGATCTT, AAGATCT, AGATCTT; from tertiary motif: CGGATCCG, CGGATCC, GGATCCG, GGATCC
```{r}
motifSets = read.table("data/motifs.txt", header=T, as.is=T)
Motifs = motifSets$Sequence
motifSets
```


# Intersect

```{r}
suppressPackageStartupMessages(library("Biostrings"))
motifSetsRev=motifSets
motifSetsRev$Sequence = apply(motifSets[2], 1, function(x){as.character(reverseComplement(DNAString(x)))})
motifSets = rbind(motifSets, motifSetsRev)
motifSets = motifSets[!duplicated(motifSets$Sequence),]
```

```{r echo=FALSE}
suppressPackageStartupMessages(library(gplots))
showProportions=function(df, totalPeaks){
  if (nrow(df)==0){
    print("No motifs in this set.")
    return()}
  # data frame 
  DF=split(df, f=df$MotifSet)
  DF=lapply(DF, function(d){d[!duplicated(d$p.name),]})
  counts=sapply(DF, nrow)
  print("Percentage of peaks in this group containing each motif:")
  print(paste(round(100*counts/totalPeaks,1), "%"))
  par(mar=c(0,0,0,0))
  venn(lapply(DF, function(d){unique(d$p.name)}))
}
```


## hits to non-binding regions
```{r}
source("../src/SigOverlap.R")
fname.motifs="intermediateFiles/MotifsGenomeLocations.bed"
fname.background = "results/BAtreatmentBackground1.bed.gz"
# make identifiers for all background regions
backgroundRegions = read.delim(fname.background, header=F)
backgroundRegions = with(backgroundRegions, paste0(V1, ":", V2, "-", V3))
totalBack=length(backgroundRegions)

cmd = paste("intersectBed -a", fname.background, "-b", fname.motifs, "-wa -wb")
# backg = read.table(pipe(cmd), sep='\t', as.is=T)
backg = robustRead(cmd)
if (nrow(backg)>0){
  names(backg) = c("p.chr", "p.start", "p.end", "m.chr", "m.start", "m.end", "m.name")
  backg$p.name = with(backg, paste0(p.chr, ":", p.start, "-", p.end))
}
# match these sequences to motif they are derived from
backg$motifLength = backg$m.end - backg$m.start
backg = merge(backg, motifSets, by.x="m.name", by.y=2, all=T)
table(backg$MotifSet)
```

```{r fig.width=3, fig.height=2.5}
showProportions(backg, totalBack)
```


## hits to binding regions
```{r}
fname.binding = "results/StandardizedWidthBAconsensusSummits.bed.gz"
# get identifiers for all binding regions
bindingRegions = read.delim(fname.binding, header=F, as.is=T)$V4
totalBind=length(bindingRegions)

cmd = paste("intersectBed -a", fname.binding, "-b", fname.motifs, "-wa -wb")
bind = robustRead(cmd)
#bind = read.table(pipe(cmd), sep='\t', as.is=T)
if (nrow(bind)>0){
  names(bind) = c("p.chr", "p.start", "p.end", "p.name", "m.chr", "m.start", "m.end", "m.name")
}
# match these sequences to motif they are derived from
bind$motifLength = bind$m.end - bind$m.start
bind = merge(bind, motifSets, by.x="m.name", by.y=2, all=T)
table(bind$MotifSet)
```
Out of the `r totalBind` regions that we are using to represent binding, `r length(unique(bind[,"p.name"]))` of them have a hit to at least one of the motifs we are looking at.

```{r fig.width=3, fig.height=2.5}
showProportions(bind, totalBind)
```


## Subsets of binding regions

Peaks were associated with genes in GeneAssignment/ConsensusPeaksToClosestGenes.Rmd. Genes associated with peaks were categorized as being up- or down-regulated by cytokinin in CompareToExpression/ExpressionSummary.Rmd. Use the results of that markdown to create sets of peaks that up- or down-regulated based on the genes they are near.
```{r}
DEpeaks=read.delim("../CompareToExpression/results/DEgenePeaksList.txt.gz", as.is=T)
```
This set includes `r nrow(DEpeaks)` rows, `r length(unique(DEpeaks$geneName))` unique genes and `r length(unique(DEpeaks$peakName))` unique peaks.

Define groups based on these.
```{r}
PeakGroups=list(backg=backgroundRegions,
                bind=bindingRegions,
                nonDE=setdiff(bindingRegions, DEpeaks$peakName),
                anyDE=unique(DEpeaks$peakName[which(!is.na(DEpeaks$direction))]),
                upReg=unique(DEpeaks$peakName[which(DEpeaks$direction > 0)]),
                downReg=unique(DEpeaks$peakName[which(DEpeaks$direction < 0)]))
TotalPeaks = sapply(PeakGroups, length)
TotalPeaks
```

### Binding regions near non-DE genes

```{r}
nonDE.binding=bind[bind$p.name %in% PeakGroups$nonDE,]
```
Out of the `r length(unique(PeakGroups$nonDE))` peaks that we are using to represent binding near non-DE genes, `r length(unique(nonDE.binding$p.name))` of them have a hit to at least one of the motifs we are looking at.

```{r fig.width=3, fig.height=2.5}
showProportions(nonDE.binding, TotalPeaks["nonDE"])
```

### Binding regions near up-regulated genes

```{r}
upReg.binding=bind[bind$p.name %in% PeakGroups$upReg,]
```
Out of the `r length(unique(PeakGroups$upReg))` peaks that we are using to represent binding near up-regulated genes, `r length(unique(upReg.binding$p.name))` of them have a hit to at least one of the motifs we are looking at.

```{r fig.width=3, fig.height=2.5}
showProportions(upReg.binding, TotalPeaks["upReg"])
```

### Binding regions near down-regulated genes

```{r}
downReg.binding=bind[bind$p.name %in% PeakGroups$downReg,]
```
Out of the `r length(unique(PeakGroups$downReg))` peaks that we are using to represent binding near down-regulated genes, `r length(unique(downReg.binding$p.name))` of them have a hit to at least one of the motifs we are looking at.

```{r fig.width=3, fig.height=2.5}
showProportions(downReg.binding, TotalPeaks["downReg"])
```

### Binding regions near any DE gene

```{r}
anyDE.binding=bind[bind$p.name %in% PeakGroups$anyDE,]
```
Out of the `r length(unique(PeakGroups$anyDE))` peaks that we are using to represent binding near DE genes (change in any direction), `r length(unique(anyDE.binding$p.name))` of them have a hit to at least one of the motifs we are looking at.

```{r fig.width=3, fig.height=2.5}
showProportions(anyDE.binding, TotalPeaks["anyDE"])
```

## Combine all data frames to a list for prcessing

With multiple groups, it will be easier to apply the same functions to each group if each group is stored as an element in one list. We will also need to store the total number of peaks in each group (including the peaks that did not have any motifs) in one object, with corresponding names.
```{r}
HITS=list(backg=backg,
          bind=bind,
          nonDE=nonDE.binding,
          anyDE=anyDE.binding,
          upReg=upReg.binding,
          downReg=downReg.binding)
```


# Count

Count how many sequnces in each group had a hit from each type of motif. For each peak, note the largest number of bases it had for each motif.
```{r}
suppressPackageStartupMessages(library("reshape"))

matchSizes = unique(bind$motifLength)
matchSizes = matchSizes[order(matchSizes)]
MotifGroups = unique(motifSets$MotifSet)

simplifyDF = function(df){
  # reshape data frame of motif-peak overlaps
  df = df[,c("p.name", "MotifSet", "motifLength")]
  df = cast(df, p.name ~ MotifSet, value="motifLength", 
            fun.aggregate = function(x){max(c(x,0))})
  row.names(df)=df$p.name
  return(df)
}

HITS2=lapply(HITS, simplifyDF)

summarizeDF = function(peakGroup){
  #
  df = HITS2[[peakGroup]]
  # set up new data frame
  df.counts = data.frame(matrix(data=NA, 
                                ncol=length(MotifGroups)+2, 
                                nrow=length(matchSizes)))
  names(df.counts)=c("binding", "minMatchSize", MotifGroups)
  df.counts$binding=rep(peakGroup, length(matchSizes))
  df.counts$minMatchSize = matchSizes
  #
  # fill in new data frame
  tp=TotalPeaks[peakGroup]
  for (i in 1:length(matchSizes)){
    min=matchSizes[i]
    # for each column (each motif) what percentage of the values are above min
    vals = apply(df[,MotifGroups], 2,
                 function(x){round(100*sum(x>=min)/tp, 2)})
    df.counts[i,MotifGroups] = vals
  }
  return(df.counts)
}

counts = do.call(rbind, lapply(names(HITS), summarizeDF))
```

Plot

```{r echo=F}
# style
main="Motif Enrichment"
mers="N-mer"
ylab=paste("% Sequences Containing", mers)
#cols=c("#6BAED6", "#2171B5", "#08306B", gray(.7), gray(.5), gray(.3))
cols=c(backg=gray(.7),
        bind="#08306B",
        nonDE=gray(.3),
        anyDE="#6BAED6",
        upReg="lightblue",
        downReg="pink")
peakGroupCols=cols #so I can use again latter, even if cols is changed
cols=cols[counts$binding]
space=c(0, 0.7)
motifLabels=c("AGATWCG", "AAGATCTT", "CGGATCCG")
ymax=round(max(as.matrix(counts[,MotifGroups]))+.5, 0)

# plot data
i=seq(1,nrow(counts),length(matchSizes)) # for using all three levels
if (mers=="7-mer") {i=grep(7, counts$minMatchSize)} # for just 7-mers
xref=barplot(as.matrix(counts[c(i),MotifGroups]),
             main=main, ylab="", ylim=c(0,ymax),
             las=1, beside=T, space=space, col=cols[i])

if (mers=="N-mer"){
  for (min in unique(counts$minMatchSize)){
    i=grep(min, counts$minMatchSize)
    xrefN=barplot(as.matrix(counts[c(i),MotifGroups]), 
                  axisnames = FALSE, axes=FALSE,
                  beside=T, space=space, col=cols[counts$binding[i]], add=T)
    label=paste0(min, "-mer")
    text(label, x=xref, y=as.matrix(counts[c(i),MotifGroups]), adj=c(0.5,-.2), cex=.7, xpd=T)
  }
}

# x-axix
  x = apply(xref, 2, mean)
  y = -.5
  text(labels = motifLabels, x = x, y = y, srt=0, xpd = T, cex=.7)
  abline(h=0)

# y-axis
mtext(ylab, side=2, line=2, cex=1.2)


# legend
legend(legend=names(cols[i]), #c("consensus summits", "background"), 
       x="topright", col=cols[i], pch=15, bty='n')



```


# Venns

Are the regions that contain secondary motifs the same regions that contain primary motifs?

Look for overlap of the three motifs among the non-binding regions.
```{r echo=FALSE}
unBound = HITS2[["backg"]]
suppressPackageStartupMessages(library(Vennerable))
prim=unBound[unBound$Primary > 0,"p.name"]
secon=unBound[unBound$Secondary > 0,"p.name"]
tert=unBound[unBound$Tertiary > 0,"p.name"]
V = Venn(list(Primary=prim, Secondary=secon, Tertiary=tert))
C3 = compute.Venn(V) # weighted version of C3
grid.newpage()
plot(C3, show=list(Faces=F))
un.overlaps = unlist(C3@IntersectionSets[c("111", "110","101", "011")])
un.overlaps = unBound[un.overlaps,]
un.percent = round(100*nrow(un.overlaps)/nrow(unBound), 1)
```

`r nrow(un.overlaps)` (`r un.percent`%) of the regions that contain any motif (`r nrow(unBound)`) contain more than one.
 
Look for overlap of the three motifs among the binding regions.
```{r echo=FALSE}
bound = HITS2[["bind"]]
prim=bound[bound$Primary > 0,"p.name"]
secon=bound[bound$Secondary > 0,"p.name"]
tert=bound[bound$Tertiary > 0,"p.name"]
V = Venn(list(Primary=prim, Secondary=secon, Tertiary=tert))
C3 = compute.Venn(V) # weighted version of C3
grid.newpage()
plot(C3, show=list(Faces=F))
b.overlaps = unlist(C3@IntersectionSets[c("111", "110","101", "011")])
b.overlaps = bound[b.overlaps,]
b.percent = round(100*nrow(b.overlaps)/nrow(bound), 1)
```

`r nrow(b.overlaps)` (`r b.percent`%) of the regions that contain any motif (`r nrow(bound)`) contain more than one.
 
 
 
 
 


Consider all of the regions that contain a given motif.  Of those, what percentage are from a given group?

```{r}
HITS3=lapply(names(HITS2), function(name){df=HITS2[[name]];
df$binding=name; return(df)})
HITS3=do.call(rbind, HITS3)
  
summarizeDFreverse = function(mGroup){
  # set up new data frame
  df.counts = data.frame(matrix(data=NA, 
                                ncol=length(HITS2)+2, 
                                nrow=length(matchSizes)))
  names(df.counts)=c("motif", "minMatchSize", names(HITS))
  df.counts$motif=rep(mGroup, length(matchSizes))
  df.counts$minMatchSize = matchSizes
  #
  # fill in new data frame
  for (i in 1:length(matchSizes)){
    min=matchSizes[i]
    df = HITS3[HITS3[[mGroup]]>=min,]
    tm=length(unique(df$p.name))
    #
    # for each column (each motif) what percentage of the values are above min
    vals = table(df$binding)[names(HITS)]
    vals = round(100*vals/tm, 2)
    df.counts[i,names(HITS)] = vals
  }
  return(df.counts)
}

reverseCounts = do.call(rbind, lapply(MotifGroups, summarizeDFreverse))
```
```{r echo=F}
suppressPackageStartupMessages(library(knitr))
kable(reverseCounts[!duplicated(reverseCounts$backg),])
```

The table above showed the percentage of regions containing a given motif that are in each category.  For example, `r reverseCounts[1,"backg"]`% of all the regions that contained the "`r reverseCounts$motif[1]`" motif are regions in the non-binding background set ("backg").  Of all the regions we examined, `r `% of them are in the background set.  If the a motif and binding had a random relationship, we would expect the percentage of motif-containing regions belonging to a given group to be similar to the percentage of total regions belonging to a given group.

For each group, note the percentage of all examined regions that are in the group.  
```{r}
# All of the sequences are either binding or not
TotalRegions = totalBind + totalBack
PercentTotalRegions = TotalPeaks/TotalRegions*100
```
```{r echo=F}
kable(data.frame(Group=names(TotalPeaks), 
                 Regions=TotalPeaks, 
                 Percent=PercentTotalRegions), digits=1)
```



We can also estimate the range of what we might reasonably expect from random selections by taking several random selections.
```{r}
N=1000
set.seed(19)
# There are regions that were tested but are not part of the results.
# We don't really need the names for these other peaks, 
# just a unique identifier, like a number.
AllRegions = c(backgroundRegions, bindingRegions)
probabilityList=list()
for (pGroup in names(HITS)){
  for (mGroup in MotifGroups){
    # make a name for storing the results
    name=paste0(mGroup, "-", pGroup)
    # sampleSize
    df = HITS3[HITS3[[mGroup]]>=0,] # select all the peaks that had this motif
    tm=length(unique(df$p.name)) #how many unquie peaks is that?
    # generate random selections and note percentage of overlap
    percents=sapply(1:N, test.per, fullPool=AllRegions,
                                  areAlso=PeakGroups[[pGroup]],
                                  sampleSize = tm)
    # store results
    probabilityList[[name]]=percents
  }
}
```



Plot

```{r echo=F}

# style
main="Motif Enrichment"
mers="N-mer"
ylab="% Motif occurences that fit each group"
#cols=c("#6BAED6", "#2171B5", "#08306B", gray(.7), gray(.5), gray(.3))
cols=1:length(unique(reverseCounts$motif))+1
names(cols)=unique(reverseCounts$motif)
cols=cols[reverseCounts$motif]
space=c(0, 0.7)
#motifLabels=c("AGATWCG", "AAGATCTT", "CGGATCCG")
ymax=round(max(as.matrix(reverseCounts[,names(HITS)]), na.rm=T)+.5, 0)

# plot data
i=seq(1,nrow(reverseCounts),length(matchSizes)) # for using all three levels
if (mers=="7-mer") {i=grep(7, counts$minMatchSize)} # for just 7-mers
xref=barplot(as.matrix(reverseCounts[i,names(HITS)]),
             main=main, ylab="", ylim=c(0,ymax),
             las=1, beside=T, space=space, col=cols[i])
# labels
if (mers=="7-mer"){
  # add text
  text("6-mer", x=xref, y=as.matrix(reverseCounts[i,names(HITS)]), adj=c(0.5,-.2), cex=.7, xpd=T)
  # add data
  i=i+1
  xref2=barplot(as.matrix(reverseCounts[i,names(HITS)]), 
                axisnames = FALSE, axes=FALSE,
                beside=T, space=space, col=cols[i], add=T)
  text("7-mer", x=xref, y=as.matrix(reverseCounts[i,names(HITS)]), adj=c(0.5,-.2), cex=.7, xpd=T)
  # add data
  i=i+1
  xref3=barplot(as.matrix(reverseCounts[i,names(HITS)]), 
                axisnames = FALSE, axes=FALSE,
                beside=T, space=space, col=cols[i], add=T)
  text("8-mer", x=xref, y=as.matrix(reverseCounts[i,names(HITS)]), adj=c(0.5,-.2), cex=.7, xpd=T)
  }

# x-axix
  x = apply(xref, 2, mean)
  y = -.5
  #text(labels = motifLabels, x = x, y = y, srt=0, xpd = T, cex=.7)
  abline(h=0)

# y-axis
mtext(ylab, side=2, line=2, cex=1.2)


# legend
legend(legend=names(cols[i]), #c("consensus summits", "background"), 
       x=xref[1,4], y=par("usr")[4], col=cols[i], pch=15, bty='n')


# Lines of Expected Random Percentages
for (i in 1:length(HITS)){
  min=min(xref[,i])
  max=max(xref[,i])
  y=PercentTotalRegions[names(HITS)[i]]
  lines(x=c(min, max), y=c(y,y))
}

# range of Expected Random Percentages
  boxplot(probabilityList, at=xref, range=0, 
          add=T, axisnames = FALSE, axes=FALSE)

```

 
 
 
Plot

```{r echo=F}
reverseCounts = reverseCounts[!duplicated(reverseCounts$motif),
                              c("motif", names(HITS))]
row.names(reverseCounts)=reverseCounts$motif
```
```{r echo=F}
# style
main="peak enrichment within motif-containing-regions"
ylab="% peak type in motif-containing-regions"
cols=peakGroupCols
cols=cols[row.names(t(reverseCounts[,names(HITS)]))]
space=c(0, 0.7)
#motifLabels=c("AGATWCG", "AAGATCTT", "CGGATCCG")
ymax=round(max(as.matrix(reverseCounts[,names(HITS)]), na.rm=T)+.5, 0)

# plot data
i=1:length(HITS)#i=seq(1,ncol(reverseCounts),length(matchSizes)) # for using all three levels
xref=barplot(as.matrix(t(reverseCounts[,names(HITS)])),
             main=main, ylab="", ylim=c(0,ymax),
             las=1, beside=T, space=space, col=cols[i])

# x-axix
  x = apply(xref, 2, mean)
  y = -.5
  #text(labels = motifLabels, x = x, y = y, srt=0, xpd = T, cex=.7)
  abline(h=0)

# y-axis
mtext(ylab, side=2, line=2, cex=1.2)


# legend
legend(legend=names(cols[i]), #c("consensus summits", "background"), 
       x="topright", col=cols[i], pch=15, bty='n')


# # Lines of Expected Random Percentages
# for (i in 1:length(HITS)){
#   min=min(xref[,i])
#   max=max(xref[,i])
#   y=PercentTotalRegions[names(HITS)[i]]
#   lines(x=c(min, max), y=c(y,y))
# }
# 
# range of Expected Random Percentages
pLnames=c()
for (mGroup in MotifGroups){
  for (pGroup in names(HITS)){
    pLnames=c(pLnames, paste0(mGroup, "-", pGroup))
  }
}
  boxplot(probabilityList[pLnames], at=xref, range=0, 
          add=T, axisnames = FALSE, axes=FALSE)

```

Boxplots indicated expectated range based on random chance.

 
 
Save results.
```{r}
for (name in names(HITS2)){
  fname=paste0("results/MaxMatchLengthByMotif_", name, ".txt")
  write.table(HITS2[[name]], file=fname, sep="\t", row.names=F, col.names=T, quote=F)
}
```

 

# Conclusions


**Session Info**
```{r housekeeping}
sessionInfo()
robustRead
```