# ARR10 binding #

To visualize this directory grab a copy of the [Integerated Genome Browser](bioviz.org).  
Select the A_thaliana_Jun_2009 genome and look in the Data Access tab for IGB Quickload / ChIP-Seq / ARR10_PRJNA263839.

## About 

Cytokinin response regulator 10 (ARR10) is a type B repsonse regulator in Arabidopsis thaliana.  In this study, two ARR10-GFP mutant lines were created in an _arr1 arr10 arr12_ loss-of-function mutant background and used in a ChIP-Seq assay to identify ARR10 binding sites.  Line a1 is cytokinin-hypersensitive. Line a2 is cytokinin-responsive.
Two replicates from line a1 and a third from line a2 were treated with cytokinin to activate binding and ChIP was performed using anti-GFP antibodies. An additional sample was made from line a2 with a mock treatment (no cytokinin).

The design and laboratory procedures were done by the [Schaller Lab](http://dartmouth.edu/biology/people/g-eric-schaller).                 
The bioinformatics for this project were done by the [Loraine lab](http://lorainelab.org/).

## Read the paper
The results of this study were published in 2017, see              
[Yan O. Zubo, Ivory Clabaugh Blakley, Maria V. Yamburenko, Jennifer M. Worthen, Ian H. Street, José M. Franco-Zorrilla, Wenjing Zhang, Kristine Hill, Tracy Raines, Roberto Solano, Joseph J. Kieber, Ann E. Loraine, and G. Eric Schaller
Cytokinin induces genome-wide binding of the type-B response regulator ARR10 to regulate growth and development in Arabidopsis
PNAS 2017 ; published ahead of print July 3, 2017, doi:10.1073/pnas.1620749114](http://www.pnas.org/content/early/2017/06/27/1620749114.abstract)

### Abstract
The plant hormone cytokinin affects a diverse array of growth and development processes and responses to the environment. How a signaling molecule mediates such a diverse array of outputs and how these response pathways are integrated with other inputs remain fundamental questions in plant biology. To this end, we characterized the transcriptional network initiated by the type-B ARABIDOPSIS RESPONSE REGULATORs (ARRs) that mediate the cytokinin primary response, making use of chromatin immunoprecipitation sequencing (ChIP-seq), protein-binding microarrays, and transcriptomic approaches. By ectopic overexpression of ARR10, Arabidopsis lines hypersensitive to cytokinin were generated and used to clarify the role of cytokinin in regulation of various physiological responses. ChIP-seq was used to identify the cytokinin-dependent targets for ARR10, thereby defining a crucial link between the cytokinin primary-response pathway and the transcriptional changes that mediate physiological responses to this phytohormone. Binding of ARR10 was induced by cytokinin with binding sites enriched toward the transcriptional start sites for both induced and repressed genes. Three type-B ARR DNA-binding motifs, determined by use of protein-binding microarrays, were enriched at ARR10 binding sites, confirming their physiological relevance. WUSCHEL was identified as a direct target of ARR10, with its cytokinin-enhanced expression resulting in enhanced shooting in tissue culture. Results from our analyses shed light on the physiological role of the type-B ARRs in regulating the cytokinin response, mechanism of type-B ARR activation, and basis by which cytokinin regulates diverse aspects of growth and development as well as responses to biotic and abiotic factors.

## Get the data
Fastq files can be downloaded from the SRA using accession [PRJNA263839](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA263839/) / [SRP048935](https://trace.ncbi.nlm.nih.gov/Traces/sra/?study=SRP048935).

The processed data can be downloaded here. This includes:

+ Aligned reads, 
+ coverage graphs, 
+ peaks called by [MACS2](https://github.com/taoliu/MACS), 
+ fold enrichment graphs (calculated by macs2 bdgcmp), and
+ consensus peaks (for the three cytokinin-treated samples).

## See the methods
The bioinformatics methods are document in a public repository:
https://bitbucket.org/lorainelab/cytochipseq


## Contact 

If you have any issues or questions with this webservice or the files located within please feel free to contact

Ivory at [ieclabau@uncc.edu](ieclabau@uncc.edu)                   
or any of the members of the [Loraine lab](http://lorainelab.org/) at UNCC.

## Files