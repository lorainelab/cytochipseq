#!/bin/sh

# use this to sort and compess bedgraph (.bdg) and bed (.bed) files output by macs2
# requires tabix, bgzip 
bdgs=`ls *.bdg`
for bdg in $bdgs
do
prefix=${bdg%.bdg}
if [ ! -s  $prefix.bdg.gz ]; # http://tldp.org/LDP/abs/html/fto.html
then
    sort -k1,1 -k2,2n $prefix.bdg | bgzip > $prefix.bdg.gz
    tabix -s 1 -b 2 -e 3 $prefix.bdg.gz
    rm $prefix.bdg
fi
done
beds=`ls *.bed`
for bed in $beds
do
prefix=${bed%.bed}
if [ ! -s  $prefix.bed.gz ]; 
then
    sort -k1,1 -k2,2n $prefix.bed | bgzip > $prefix.bed.gz
    tabix -s 1 -b 2 -e 3 $prefix.bed.gz
    rm $prefix.bed
fi
done
fs=`ls *.narrowPeak`
for f in fs
do
    gzip $fs
done