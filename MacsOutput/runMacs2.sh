#!/bin/sh

G=119667750 # Arabidopsis
M=/Users/pi/bin/MACS2-2.1.0.20140616/bin/macs2 #Ann's laptop
Q=0.01
CMD="$M callpeak -f BAM -B -g $G -q $Q --keep-dup auto" 
EXPS="Exp1_Rep1 Exp1_Rep2 Exp2_Mock Exp2_BA"
for N in $EXPS
do
    #echo $CMD
    $CMD -t ${N}_IP.bam -c ${N}_Input.bam -n $N 2>&1 | tee $N.log
    echo "Starting indexing." 
    compressMacs2Files.sh
    echo "Done indexing."
done

# this was useful:
# http://www.thegeekstuff.com/2011/07/bash-for-loop-examples/