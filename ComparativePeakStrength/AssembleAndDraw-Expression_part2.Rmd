---
title: "Plot fold erichment for BA vs Mock for golden list genes"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction

This mark down is similar to AssembleAndDraw-Expression.Rmd, which was exploratory.  Here, the objective is to produce a figure showing the strength of binding in the BA and Mock samples (in Exp2) in peaks near DE genes, specifically genes on the golden list.

## Questions

 * Do we see any shift in binding in the BA treated sample compared to the Mock sample if we only consider peaks that were assigned to up- or down-regulated genes?   
 
 Unlike the previous markdown that this is based on, this will only consider peaks near the 804 genes that have been identified as putative binding targets (genes differentially expressed  in response to BA treatment and near a peak).

```{r}
source("../src/ProjectVariables.R")
```

## Read the data

Read the table that was assembled in AssembleAndDraw-Expression.Rmd, relating peaks, genes and gene expression.
```{r}
BApeaks = read.delim("results/AssembledBAsummitsFEandExpression.txt.gz")
```

Read peaks related to expression changes.
```{r}
DEpeaks = read.delim("../CompareToExpression/results/DEgenePeaksList.txt.gz", as.is=T)[c(1,2,4,5,7,18)]
```
This includes `r length(unique(DEpeaks$geneName))` unique genes linked to `r length(unique(DEpeaks$peakName))` unique peaks (in `r nrow(DEpeaks)` rows).

Limit BApeaks to the set of peaks that are in the DEpeaks list.
```{r}
BApeaks = BApeaks[BApeaks$category != "nonDE",]
```
Now we have this data for only the `r nrow(BApeaks)` peaks that have been linked to DE genes.

Read peaks related to golden list genes.
```{r}
GLpeaks = read.delim("../CompareToExpression/results/GoldenListGenesToBinding.txt.gz", as.is=T)
GL = split(GLpeaks, f=GLpeaks$direction)
```
This file includes `r length(unique(GLpeaks$gene))` unique genes that were associated with `r length(unique(GLpeaks$peakName))` unique peaks (in `r nrow(GLpeaks)` rows).
`r length(unique(GL[["Up"]]$gene))` of these genes are said to be "Up" and `r length(unique(GL[["Down"]]$gene))` are "Down".  Those genes are associated with `r length(unique(GL[["Up"]]$peakName))` and `r length(unique(GL[["Down"]]$peakName))` peaks, respectively.


# Analysis

Make a function go draw the base plot - the values for all peaks near DE genes are drawn in gray as a frame of reference.  Points representing only up- or down-regulated genes in will be plotted over this.
```{r fig.width=4, fig.height=8}
transgray = rgb(.5,.5,.5,.7) #rgb(.5,.5,.5,.1)
transblue = rgb(0,0,1,.7) #rgb(0,0,1,.3)
fig.gold = rgb(255/255,202/255,108/255,1) # use for down-reg
fig.blue = rgb(32/255,121/255,178/255,1) # use for up-reg
#lims=c(0,15)
addUnityLine = function(){abline(0,1,lty=2, col=gray(.5))}

drawBase = function(main = "Enrichment in BAsummits in Exp2 samples"){
  plot(BApeaks$Mock, BApeaks$BA, xlim=c(0,10), ylim=c(0,23),pch=16, col=transgray,
       xlab = "Fold Erichment in Mock sample", ylab = "Fold Enrichment in BA sample",
       main = main, las=1, asp=1, xpd=T)
  addUnityLine()
}

addPoints = function(peaks, col=rgb(0,0,1,.3)){
  points(BApeaks[peaks,"Mock"], BApeaks[peaks,"BA"], pch=16, col=col)
}

drawBase()
```


## Plot peaks near DE genes

Draw all up-regulated genes.
```{r fig.width=4, fig.height=8}
upPeaks = which(BApeaks$category == "up")
drawBase("All peaks near up-regulated genes")
addPoints(upPeaks, col=fig.blue)
```





```{r fig.width=4, fig.height=8, echo=FALSE}
downPeaks = which(BApeaks$category == "down")
drawBase("Peaks near down-regulated genes")
addPoints(downPeaks, col=fig.gold)
```

## Plot peaks near golden list genes

Color only the golden list genes.

```{r fig.width=4, fig.height=8, echo=FALSE}
goldenUP = which(BApeaks$name %in% GL[["Up"]]$peakName)
drawBase("Peaks near up-regulated golden list genes")
addPoints(goldenUP, col=fig.blue)
```


```{r fig.width=4, fig.height=8, echo=FALSE}
goldenDOWN = which(BApeaks$name %in% GL[["Down"]]$peakName)
drawBase("Peaks near down-regulated golden list genes")
addPoints(goldenDOWN, col=fig.gold)
```

## Composit figure

Finally, these should all be panels in a single figure.

```{r fig.width=4, fig.height=8, echo=FALSE}
par(mfcol=c(2,2), oma=c(4,2,2,0))

# top left
par(mar=c(0,2,3,0))
drawBase("DE genes")
addPoints(upPeaks, col=fig.blue)
mtext("up-regulated", side=2, line=2)
# bottom left
par(mar=c(3,2,0,0))
drawBase("")
addPoints(downPeaks, col=fig.gold)
mtext("down-regulated", side=2, line=2)

# top right
par(mar=c(0,0,3,2))
drawBase("golden list genes")
addPoints(goldenUP, col=fig.blue)
# bottom right
par(mar=c(3,0,0,2))
drawBase("")
addPoints(goldenDOWN, col=fig.gold)

mtext("Binding strength of peaks near", outer=T, side=3)
mtext("Fold Enrichment in BA Sample", side=2, line=1, outer=T)
mtext("Fold Enrichment in Mock Sample", side=1, outer=T)

par(mfrow=c(1,1))
```

## Only golden list genes

A simpler way to get this idea across, using just one image would be to only look at the golden list set, with color distinguishing the two sets on the same plot.

```{r fig.width=4, fig.height=8, echo=FALSE}
goldenUP = which(BApeaks$name %in% GL[["Up"]]$peakName)
drawBase("Peaks near DE genes")
addPoints(goldenUP, col=fig.blue)
addPoints(goldenDOWN, col=fig.gold)
legend(x="bottomright", legend=c("DE (not golden list)", "up-reg.", "down-reg."), col=c(transgray, fig.blue, fig.gold), pch=16)
```




# Still to do:

 * I need to decide if the range of the plot should be cut back.  If the y-axis only goes to 15, then we loose 3 points that might be peaks of interest but the plot has a lot less white space and we don't want the outliers to steal the show. Cutting off everything where y<2 will also remove white space. Re-do sizing accordingly.
 * If using the composite figure, I need to remove the extra axis labels that cut into the neighboring panel.
 * I need to export these as high res (>=100 dpi) images in png format so that Eric can insert them into his program of choice.
 * Check that both colors stand out against the gray (even in black-and-white). We might need to make the gray a lighter shade and/or a different point type.




```{r}
sessionInfo()
```

