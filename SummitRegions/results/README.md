# SummitRegions/results README #

Summit regions are a version of peaks that have constricted to a tighter range (about 200 bp) surrounding the original peak.  These regions are used to link peaks from different samples.  Bed format is a convenient way to store sets of regions because it can be readily visualized using genome browsing software such as Integrated Genome Browser (IGB).

### Made by DefiningSuperSummits.Rmd

**AllSamplesSummitRegions.bed.gz** - summit regions for all peaks from all samples.

**SuperSummitRegions.bed.gz** - summit regions from AllSamplesSummitRegions.bed.gz where overlapping summit regions have been merged into a single unit.

### Made by BASuperSummits.Rmd

**BASuperSummits.bed.gz** - This set of summit regions is similar to SuperSummitRegions.bed.gz, but this one only considers samples from the BA treated condition.

**BASharedSuperSummits.bed.gz** - A filtered version of BASuperSummits.bed.gz that only includes the regions that were derived from all three BA samples.  This set is subsequently used as the consensus peaks for the treated condition.

### Made by MockSuperSummits.Rmd

**MockUniqueSummits.bed.gz** - summit regions taken from AllSamplesSummitRegions.bed.gz and filtered for only mock peaks and only those that do not overlap with any BA peaks.


