README for SummitRegions folder

The mark down DefiningSuperSummits.Rmd defines the summit regions used through the rest of the project based on the four samples.

The mark down BASuperSummits.Rmd does almost exactly the same thing as DefiningSuperSummits.Rmd, but it only considers the BA samples.
