#!/bin/bash

# Run this script to create bed files of all
# homer motif files in the current working directory.

# Requires that homer, bedtools, and gzip be available in the system path.
# Requires the gneome in fasta format

GENOME="../../../ExternalDataSets/A_thaliana_Jun_2009.fasta"

for MOTIF in $(ls *.motif)
do
NAME=${MOTIF%.motif}
scanMotifGenomeWide.pl $MOTIF $GENOME -bed | sortBed | gzip > bed/$NAME.bed.gz
done
