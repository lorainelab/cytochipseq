---
title: "Compare as a group for each sample"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction
This is different from the ComparingBySummits and ComparingByPeaks markdowns, where we needed to matching up peaks from different samples in order to compare them in terms of matched up regions.  Here, we don't need to worry about matching them up between samples because we are looking at each set separately in terms of the group as a whole.  

## Questions

 * What is the distribution of fold enrichment values for each samples peaks?
 * What is the distribution of -log(q-value) for each samples peaks?
 * Does it look like the Mock represents generally less binding than the BA samples?

# Each samples' peaks

Get the values of variables.
```{r}
source("../src/ProjectVariables.R")
samples=getSampleNames() # see projectVariables
samples=samples[order(samples)]
minPeakQ=getMinPeakQ()
minFE = 2.5

cols=getSampleColors() # from ProjectVariables.R

# super summits file name
ss.fname = "../SummitRegions/results/SuperSummitRegions.bed"
```


### All Peaks

Look at all peaks.  
```{r}
peaks = read.delim("../PeakProcessing/results/peaks.tsv.gz")
#peaks = peaks[peaks$neg.log10.qvalue >= minPeakQ,]
Peaks = split(peaks, f=peaks$sample)
library(knitr)
```

**total number of peaks per sample** when considering all peaks
```{r echo=FALSE}
kable(t(data.frame(numPeaks=sapply(Peaks, nrow))), format.args=list(big.mark=","))
```

Consider the **fold enrichment**.  Recall that fold enrichment is a way to measure how _much_ binding there is in a given region. Its calculated based on the ratio of reads from the IP sample over the reads from the input sample.
```{r}
getAllFC = function(df){
  return(df$fold_enrichment)
}
peakFC = lapply(Peaks, getAllFC)

df = rbind(mean = sapply(peakFC, mean),
           median = sapply(peakFC, median),
           min = sapply(peakFC, min),
           max = sapply(peakFC, max))
```

**fold enrichment** summary when considering all peaks
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
tt=t.test(peakFC[["Exp2_BA"]], peakFC[["Exp2_Mock"]])
tt
tt4=t.test(c(peakFC[["Exp2_BA"]],peakFC[["Exp1_Rep1"]],peakFC[["Exp1_Rep2"]]), peakFC[["Exp2_Mock"]])
```

The values from the BA and Mock treatments from experiment 2 are different. (p-value: `r tt$p.value`)
The values from all the BA samples vs the Mock are different with a p-value of `r tt4$p.value`.

```{r echo=FALSE, fig.width=4}
boxplot(peakFC, las=1, col=cols, 
        ylab="fold enrichment", main="fold enrichment summary")
boxplot(peakFC, ylim=c(1,5), las=1, col=cols,
        ylab="fold enrichment")
# boxplot(lapply(peakFC,log), las=1, col=cols,ylab="log(FC)")
```

Consider the **q-value**.  Recall that the q-value is a measure of how confident we are that a given peak is _real_, its an adjusted p-value. Peaks are only reported if they have a q-value of 0.01 or less (less being more significant). Many peaks have such small p-values, that it is most effective to look at the negative log of the q-value, which in turn would have a minimum of 2 (-log(0.01)) and higher numbers are more statistically significant.  When we filtered peaks to make the BA peaks (consensus peaks) we only used peaks with a -log(q) of 10 or more. 
```{r}
getAllQ = function(df){
  return(df$neg.log10.qvalue)
}
peakQ = lapply(Peaks, getAllQ)

df = rbind(mean = sapply(peakQ, mean),
           median = sapply(peakQ, median),
           min = sapply(peakQ, min),
           max = sapply(peakQ, max))
```

**-log(q-value)** summary when considering all peaks
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
```

```{r echo=FALSE, fig.width=4}
boxplot(peakQ, las=1, col=cols, 
        ylab="-log(Qvalue)", main="Q value summary")
boxplot(peakQ, ylim=c(0,200), las=1, col=cols,
        ylab="-log(Qvalue)")
boxplot(peakQ, ylim=c(0,20), las=1, col=cols,
        ylab="-log(Qvalue)")
boxplot(lapply(peakQ,log), las=1, col=cols,
        ylab="log(-log(Qvalue))")
```


### All peaks with Qval over `r minPeakQ`

The above considers all peaks. If we only consider the peaks with a -log(q-value) of `r minPeakQ` or better (the peaks we used to create the summit regions and consensus peaks) then the tables for fold enrichment and q-values look like this:

```{r echo=FALSE}
peaks = read.delim("../PeakProcessing/results/peaks.tsv.gz")
peaks = peaks[peaks$neg.log10.qvalue >= minPeakQ,]
Peaks = split(peaks, f=peaks$sample)
```

**total number of peaks per sample** when limited to peaks with -log(q-value) >= `r minPeakQ`
```{r echo=FALSE}

kable(t(data.frame(numPeaks=sapply(Peaks, nrow))), format.args=list(big.mark=","))

# fc
peakFC = lapply(Peaks, getAllFC)
df = rbind(mean = sapply(peakFC, mean),
           median = sapply(peakFC, median),
           min = sapply(peakFC, min),
           max = sapply(peakFC, max))
```

**fold enrichment** summary when limited to peaks with -log(q-value) >= `r minPeakQ`.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
tt=t.test(peakFC[["Exp2_BA"]], peakFC[["Exp2_Mock"]])
```

The values from the BA and Mock treatments from experiment 2 are different. (p-value: `r tt$p.value`)

```{r echo=FALSE}
# q-vals
peakQ = lapply(Peaks, getAllQ)
df = rbind(mean = sapply(peakQ, mean),
           median = sapply(peakQ, median),
           min = sapply(peakQ, min),
           max = sapply(peakQ, max))
```

**-log(q-value)** summary when limited to peaks with -log(q-value) >= `r minPeakQ`.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
```


### All peaks with fold enrichment of `r minFE` or more

If we only consider the peaks with a fold enrichment of `r minFE` or better then the tables for fold enrichment and q-values look like this:

```{r echo=FALSE}
peaks = read.delim("../PeakProcessing/results/peaks.tsv.gz")
peaks = peaks[peaks$fold_enrichment >= minFE,]
Peaks = split(peaks, f=peaks$sample)
```

**total number of peaks per sample** when limited to peaks with fold enrichment >= `r minFE`.
```{r echo=FALSE}

kable(t(data.frame(numPeaks=sapply(Peaks, nrow))), format.args=list(big.mark=","))

# fc
peakFC = lapply(Peaks, getAllFC)
df = rbind(mean = sapply(peakFC, mean),
           median = sapply(peakFC, median),
           min = sapply(peakFC, min),
           max = sapply(peakFC, max))
```

**fold enrichment** summary when limited to peaks with fold enrichment >= `r minFE`.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
tt=t.test(peakFC[["Exp2_BA"]], peakFC[["Exp2_Mock"]])
```

The values from the BA and Mock treatments from experiment 2 are different. (p-value: `r tt$p.value`)

```{r echo=FALSE}
# q-vals
peakQ = lapply(Peaks, getAllQ)
df = rbind(mean = sapply(peakQ, mean),
           median = sapply(peakQ, median),
           min = sapply(peakQ, min),
           max = sapply(peakQ, max))
```

**-log(q-value)** summary when limited to peaks with fold enrichment >= `r minFE`.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
```

### All peaks with fold enrichment of `r minFE` or more and Qval of `r minPeakQ` or better

If we only consider the peaks with a fold enrichment of `r minFE` or better then the tables for fold enrichment and q-values look like this:

```{r echo=FALSE}
peaks = read.delim("../PeakProcessing/results/peaks.tsv.gz")
peaks = peaks[peaks$fold_enrichment >= minFE,]
peaks = peaks[peaks$neg.log10.qvalue >= minPeakQ,]
Peaks = split(peaks, f=peaks$sample)
```

**total number of peaks per sample** when limited to peaks with fold enrichment of `r minFE` or more and Qval of `r minPeakQ` or better.
```{r echo=FALSE}

kable(t(data.frame(numPeaks=sapply(Peaks, nrow))), format.args=list(big.mark=","))

# fc
peakFC = lapply(Peaks, getAllFC)
df = rbind(mean = sapply(peakFC, mean),
           median = sapply(peakFC, median),
           min = sapply(peakFC, min),
           max = sapply(peakFC, max))
```

**fold enrichment** summary when limited to peaks with fold enrichment of `r minFE` or more and Qval of `r minPeakQ` or better.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
tt=t.test(peakFC[["Exp2_BA"]], peakFC[["Exp2_Mock"]])
```

The values from the BA and Mock treatments from experiment 2 are different. (p-value: `r tt$p.value`)


```{r echo=FALSE}
# q-vals
peakQ = lapply(Peaks, getAllQ)
df = rbind(mean = sapply(peakQ, mean),
           median = sapply(peakQ, median),
           min = sapply(peakQ, min),
           max = sapply(peakQ, max))
```

**-log(q-value)** summary when limited to peaks with fold enrichment of `r minFE` or more and Qval of `r minPeakQ` or better.
```{r echo=FALSE}
kable(df, digits=1, format.args=list(big.mark=","))
```

# Conclusions

# Limitations
## a note about qvals 
Don't forget: the q-value is dependent on the sequencing depth.  The Mock sample seems to have markedly lower q-values. This may be due to the fact that the Mock sample had markedly fewer reads.  The fewer reads may have been the result of less binding, and so less DNA is pulled down, and less DNA is avaible to make a library from.  On the other hand, the fold enrichment overall is not markedly lower in the Mock, although the top handful of values in the BA treated samples are much higher than any of the Mock treated values.

See ../Alignment/NumberOfReads.xlsx

```{r echo=FALSE}
suppressPackageStartupMessages(library(xlsx))
kable(read.xlsx("../Alignment/NumberOfReads.xlsx", sheetIndex=1),
      format.args=list(big.mark=","))
```

Session Info
```{r}
sessionInfo()
```

