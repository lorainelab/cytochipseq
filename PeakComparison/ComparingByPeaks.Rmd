---
title: "Compare super summits to overlapping peaks"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---
```{r include=FALSE}
knitr::opts_chunk$set(fig.path = 'figure/')
```
**Date run: `r date()`**

# Introduction

We have already defined a set of short regions called super summits.  These are regions that have at least one summit from at least one sample.  Now we want to know:

* How many of the super summits have binding from all samples?
* How well do replicates agree with other about where peaks should be?  
* In particular, is the control sample from Batch 2 giving us a subset of the peaks we see in the treatment sample?

If we want to make a Venn diagram showing how many of the super summits that had binding in one sample also had it in another, then a peak overlapping a given super summit is enough to say "yes, this site had binding in this sample".  A single peak could overlap several super summits.  Here, we'll allow a peak to match any and all regions it overlaps.  

# Method

**Requires bedtools 2.6.0 or later with interesectBed in PATH.**

## Find input files
Establish which files have the data we need. We need the super summits and we need bed files describing the peaks from each sample.
```{r}
source("../src/ProjectVariables.R")
samples=getSampleNames() # see projectVariables
samples=samples[order(samples)]
minPeakQ=getMinPeakQ()

# get the file names for the bed file for each sample
path="../PeakProcessing/results/"
sampleFiles = paste0(path, dir(path=path, pattern="bed"))
sampleFiles=sampleFiles[order(sampleFiles)]
names(sampleFiles)=samples

# super summits file name
ss.fname = "results/SuperSummitRegions.bed"
```

## Determine where super summits overlap with peaks

Make a list "Hits" to hold the outputs from comparing the full size super summits to the regions of interest. 
```{r}
Hits = as.list(samples)
names(Hits) = samples
```

Use bedtools to test for intersection. Do this for each type of region. Store the results in Hits.  Run bedtools intersectBed on full size super summits and read in results and get the overlapping super summits for each sample.  read.table will read in the description of this file and separate on the spaces, making the q value field easier to extract.  Only keep the overlaps that involve a peak with a negLog10 Q value of at least `r minPeakQ`.
```{r}
for (I in samples){
  cmd=paste("intersectBed -a", ss.fname, "-b", sampleFiles[I], "-wa -wb")
  Hits[[I]] = read.table(pipe(cmd), stringsAsFactors = F)[,c(4,21)]
  names(Hits[[I]]) = c("SuperSummit", "q")
  Hits[[I]]$q = as.numeric(gsub(";", "", Hits[[I]]$q))
  Hits[[I]] = Hits[[I]][Hits[[I]]$q>=minPeakQ,]
  Hits[[I]] = Hits[[I]][ ,"SuperSummit"]
}
```
Hits has an element for each sample.  Each element is a vector of the super summits that overlapped with a peak in the sample.

## Venn Diagrams

Make Venn diagram of the experiment 1 samples.
```{r }
suppressPackageStartupMessages(library("Vennerable")) ## from R-forge
cols=getSampleColors() # from ProjectVariables.R
```

### Experiment 1

Make Venn diagram of the experiment 2 samples.
```{r fig.width=4, fig.height=4}
V = Venn(Hits[1:2]) #an object of class Venn to use with Vennerable package
exp1C2 = compute.Venn(V) # weighted version of exp1C2
theme = VennThemes(exp1C2) 
for (i in 1:2){
  theme$SetText[[i]]$col = cols[i]
  theme$Set[[i]]$col = cols[i]
  #theme$SetText[[i]]$fontsize = 14
}
exp1C2@SetLabels$hjust[2] = "left"
exp1C2@SetLabels$hjust[1] = "right"
exp1C2Tallies = sapply(exp1C2@IntersectionSets, length)
# plot
grid.newpage()
plot(exp1C2, gpList = theme, show=list(Faces=F))
```

Of the `r sum(exp1C2Tallies)` super summits that hit a peak in either of the experiment 1 samples, `r exp1C2Tallies['11']` of them (`r round(100*exp1C2Tallies['11']/sum(exp1C2Tallies), 0)`%) are shared by the two samples.  This overlap makes up `r round(100*exp1C2Tallies['11']/sum(exp1C2Tallies[c('01','11')]), 0)`% of the smaller set.

### Experiment 2

Make Venn diagram of the experiment 2 samples.
```{r fig.width=5}
V = Venn(Hits[3:4]) #an object of class Venn to use with Vennerable package
exp2C2 = compute.Venn(V) # weighted version of exp1C2
theme = VennThemes(exp2C2) 
for (i in 1:2){
  theme$SetText[[i]]$col = cols[i+2]
  theme$Set[[i]]$col = cols[i+2]
  #theme$SetText[[i]]$fontsize = 14
}
exp2C2@SetLabels$hjust[2] = "left"
exp2C2@SetLabels$hjust[1] = "right"
exp2C2Tallies = sapply(exp2C2@IntersectionSets, length)
# plot
grid.newpage()
plot(exp2C2, gpList = theme, show=list(Faces=F))
```

Of the `r sum(exp2C2Tallies)` super summits that hit a peak in either of the experiment 1 samples, `r exp2C2Tallies['11']` of them (`r round(100*exp2C2Tallies['11']/sum(exp2C2Tallies), 0)`%) are shared by the two samples.  This overlap makes up `r round(100*exp2C2Tallies['11']/sum(exp2C2Tallies[c('01','11')]), 0)`% of the smaller set.

### All BA samples

Make Venn diagram of the three BA treated samples. Make one that is not weighted so the numbers are clear.
```{r fig.width=3, fig.height=3}
V = Venn(Hits[1:3]) #an object of class Venn to use with Vennerable package
baC3 = compute.Venn(V, doWeights = F) # non-weighted version of exp1C2
theme = VennThemes(baC3) 
for (i in 1:3){
  theme$SetText[[i]]$col = cols[i]
  theme$Set[[i]]$col = cols[i]
  #theme$SetText[[i]]$fontsize = 14
}
baC3@SetLabels$hjust[2] = "left"
baC3@SetLabels$hjust[3] = "right"
grid.newpage()
plot(baC3, gpList = theme, show=list(Faces=F))
```

Now make one that is weighted so the visual is helpful.
```{r fig.width=4, fig.height=4}
baC3 = compute.Venn(V) # weighted version of exp1C2
theme$FaceText = lapply(theme$FaceText, function(x){x$col=NA; return(x)})
theme$FaceText[["111"]]$col = "black"
baC3@SetLabels$hjust[2] = "left"
baC3@SetLabels$hjust[3] = "right"
SectionTallies = sapply(baC3@IntersectionSets, length)
# plot
grid.newpage()
plot(baC3, gpList = theme, show=list(Faces=F))
```

Of the `r sum(SectionTallies)` super summits that overlapped a peak in at least one BA samples, `r SectionTallies['111']` of them (`r round(100*SectionTallies['111']/sum(SectionTallies), 0)`%) overlap a peak in all three BA samples.  Many of the others are shared by at least two samples.  Only `r round(100*sum(SectionTallies[c('100','010','001')])/sum(SectionTallies), 0)`% are found in only one sample (`r round(100*SectionTallies['100']/sum(SectionTallies), 0)`% from `r samples[1]`, `r round(100*SectionTallies['010']/sum(SectionTallies), 0)`% from `r samples[2]`, and `r round(100*SectionTallies['001']/sum(SectionTallies), 0)`% from `r samples[3]`).

### All BA samples vs mock

```{r fig.width=6}
BA_samples <- unique(c(Hits[[1]],Hits[[2]],Hits[[3]]))
V = Venn(list(BA_samples=BA_samples,Exp2_Mock=Hits[[4]])) 
treatC2 = compute.Venn(V) # weighted version of exp1C2
theme = VennThemes(treatC2) 
colors = c(grey(0), cols[4])
for (i in 1:2){
  theme$SetText[[i]]$col = colors[i]
  theme$Set[[i]]$col = colors[i]
  #theme$SetText[[i]]$fontsize = 14
}
treatC2@SetLabels$hjust[2] = "left"
treatC2@SetLabels$hjust[1] = "right"
treatC2Tallies = sapply(treatC2@IntersectionSets, length)
# plot
grid.newpage()
plot(treatC2, gpList = theme, show=list(Faces=F))
```

Of the `r sum(treatC2Tallies)` super summits that hit a peak in any sample (which must be all of them), `r treatC2Tallies['11']` of them (`r round(100*treatC2Tallies['11']/sum(treatC2Tallies), 0)`%) are shared by the mock treated and at least one of the BA samples.  This overlap makes up `r round(100*treatC2Tallies['11']/sum(treatC2Tallies[c('01','11')]), 0)`% of the super summits found in the mock treated. I think anything below 85% (100% would be technically correct, but we'll allow wiggle room)is too small for the smaller set to considered a subset of the larger one.

### All four samples individually

```{r}
V = Venn(Hits) #an object of class Venn to use with Vennerable package
C4 = compute.Venn(V, doWeights = F) # non-weighted version of exp1C2
theme = VennThemes(C4) 
for (i in 1:4){
  theme$SetText[[i]]$col = cols[i]
  theme$Set[[i]]$col = cols[i]
  #theme$SetText[[i]]$fontsize = 14
}
C4Tallies = sapply(C4@IntersectionSets, length)
grid.newpage()
plot(C4, gpList = theme, show=list(Faces=F))
```

## BA Summits

Consider only the super summits that are defined by one or more summits from a BA sample.
```{r}
BAsamples = getBAsampleNames()
BAhits = Hits[BAsamples]
for (sample in BAsamples){
  names = BAhits[[sample]]
  names = gsub("Exp2_Mock_peak_[0-9]*[,]?", "", names)
  names = gsub("[,]?$", "", names)
  names = names[nchar(names)>0]
  BAhits[[sample]] = names
}
```

Make Venn diagram of the three BA treated samples. Make it weighted so the visual is helpful.
```{r fig.width=4, fig.height=4}
V = Venn(BAhits[BAsamples])
BAC3 = compute.Venn(V) 
theme = VennThemes(BAC3)
for (i in 1:3){
  theme$SetText[[i]]$col = cols[i]
  theme$Set[[i]]$col = cols[i]
}
theme$FaceText = lapply(theme$FaceText, function(x){x$col=NA; return(x)})
theme$FaceText[["111"]]$col = "black"
BAC3@SetLabels$hjust[2] = "left"
BAC3@SetLabels$hjust[3] = "right"
SectionTallies = sapply(BAC3@IntersectionSets, length)
# plot
grid.newpage()
plot(BAC3, gpList = theme, show=list(Faces=F))
```

Of the `r sum(SectionTallies)` super summits that include a summit in at least one of the BA samples, `r SectionTallies['111']` of them (`r round(100*SectionTallies['111']/sum(SectionTallies), 0)`%) overlap a peak in all three BA samples.  Many of the others are shared by at least two samples.  Only `r round(100*sum(SectionTallies[c('100','010','001')])/sum(SectionTallies), 0)`% are found in only one sample (`r round(100*SectionTallies['100']/sum(SectionTallies), 0)`% from `r samples[1]`, `r round(100*SectionTallies['010']/sum(SectionTallies), 0)`% from `r samples[2]`, and `r round(100*SectionTallies['001']/sum(SectionTallies), 0)`% from `r samples[3]`).

# Conclusions

There are `r C4Tallies['1111']` super summits that are shared by all four samples when we compare them in this way. `r treatC2Tallies['11']` are shared by all BA samples.

The samples in Experiment 1 are much more similar to each other than the two samples from experiment 2.  In any comparison that does not include the mock sample, there are considerably more sites in common than sites that are different.

The mock sample extends to more super summits than we would expect to see if it was simply a subset of the super summits reached by peaks in the treated samples.


**HouseKeeping**
```{r}
sessionInfo()
```