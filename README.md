# ARR10 binding with BA treatment #

This repository contains data files and source code used to analyze
results from a ChIP-Seq experiment in which three-week old Arabidopsis thaliana plants were treated by immersion in in media 5 microMolar BA. Samples included shoot material only.

Code used for analysis, making figures, and making data files for
visualization in external site and programs are grouped into folders
with names indicating their common purpose or analysis type. 

Each module folder is designed to be run as a relatively
self-contained project in RStudio. As such, each folder contains an
".Rproj" file. To run the code in RStudio, just open the .Rproj file
and go from there. However, note that the code expects Unix-style file
paths and depends on external libraries, mainly from Bioconductor. 

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are version-controlled
in ExternalDataSets but may also be available from external sites.

This project is a collaboration between the Kieber Lab at UNC Chapel
Hill, the Schaller Lab at Dartmouth University, and the Loraine Lab at
the North Carolina Research Campus (and UNC Charlotte).
<fill in funcding info>

* * *

# What's here 

Analysis modules and other directories include .Rmd files (R
Markdown), output from knitting Rmd files in HTML (Web pages), folders
containing results (results) and (sometimes) folders containing
externally supplied data used only in one module (data). Results folders
also contain high-quality image files suitable for inclusion in slides or publications. 

* * *

## ExternalDataSets

Data files from external sources, e.g., TAIR10.bed.gz

* * *

## GeneStructures

Sample files showing how to manipulate data from BED files. 

* * *

## MacsOutput

Output from running macs2 on ChIP-Seq alignment (BAM) files.
Script used to run macs2.
Script used to compress macs2 bed, bedgraph files.

* * *

## PeakProcessing

Merges peaks data into a single file.
Adds new column: sample, indicating what sample the peak is from.

* * *

## PeakComparison

Uses bedtools merge to create super-summits, merged sumitt regions from peaks.
Builds plots showing correlations between peak fold-enrichment between samples.

* * *

## Alignment files

This directory contains BAM alignment files. File names indicate the sample.

IP - sequences from ChIP'd DNA
Input - input DNA (not ChIP'd)
Exp1 - Data from hypersensitive line 2-6 (2 reps, BA-treated)
Exp2 - Data from line 2-17 (BA and mock-treated)

For files, go to:
http://bit.ly/arr10bam


* * *

# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley ieclabau@uncc.edu

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT