Reading gene models
===================

Introduction
------------

The goal of this Markdown document is investigate how easy it is 
(or not) to read BED files into R for data analysis.

I'd like to try methods described in rTracklayer and related packages as described here:

* https://www.biostars.org/p/82725/

I'll use bed detail files from the IGBQuickLoad site, starting with
TAIR10.bed.gz.

A related post:

* https://www.biostars.org/p/74741/
* http://bedops.readthedocs.org/en/latest/content/reference/set-operations/closest-features.html

Analysis
--------

```{r}
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(ChIPpeakAnno))
```

Read gene models:

```{r}
fname='../ExternalDataSets/TAIR10.bed.gz'
library(utils)
cmd=paste('gunzip -c',fname,'| cut -f1-12')
conn=pipe(cmd)
gene_models=import.bed(conn,genome='A_thaliana_Jun_2009')
```

Read super summits:

```{r}
fname=dir('../PeakComparison/results/',pattern='super_summits')[1]
super_summits=import.bed(conn,genome='A_thaliana_Jun_2009') # doesn't work
```

TODO: add sequence sizes?
