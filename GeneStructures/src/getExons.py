#!/usr/bin/env python

ex=\
"""
Read a BED file and extract exon features. 
Reads from stdin or one or more files.
Writes exon feature in BED14 to stdout.

example)

  gunzip -c TAIR10.bed.gz | %prog > out.tsv
"""

import sys,fileinput,optparse,re


tab='\t'
nl='\n'
reg=re.compile(r'\.\d+$')

def main(args):
    for line in fileinput.input(args):
        # ignore track line headers that are sometimes at the start
        # of a file
        if line.startswith('track'):
            continue
        newlines = makeExonLines(line)
        for newline in newlines:
            sys.stdout.write(newline+'\n')

def makeExonLines(line):
    (chrom,chromStart,name,score,strand,itemRGB,blockSizes,
     blockStarts)=parseBedLine(line)
    i = 0
    newlines=[]
    chromStart=int(chromStart)
    for blockStart in blockStarts:
        exon_size=blockSizes[i]
        exon_start=blockStart+chromStart
        exon_end=exon_start+exon_size
        exon_name = makeExonName(start=exon_start,end=exon_end,seqname=chrom)
        newline = tab.join([chrom,str(exon_start),
                            str(exon_end),exon_name,score,
                            strand])
        newlines.append(newline)
        i = i + 1
    return newlines
            

def parseBedLine(line):
    toks = line.rstrip().split('\t')
    # see: http://genome.ucsc.edu/FAQ/FAQformat.html#format1
    chrom=toks[0]
    chromStart = int(toks[1])
    name=toks[3]
    score=toks[4]
    strand=toks[5]
    itemRGB=toks[8]
    # UCSC beds put , at end of fields
    # TopHat doesn't
    if toks[10].endswith(','):
        toks[10]=toks[10][0:-1]
    blockSizes=map(lambda x:int(x),toks[10].split(','))
    if toks[11].endswith(','):
        toks[11]=toks[11][0:-1]
    blockStarts=map(lambda x:int(x),toks[11].split(','))        
    return (chrom,chromStart,name,score,strand,itemRGB,blockSizes,
            blockStarts)

def makeExonName(start=None,
                   end=None,
                   seqname=None):
    """
    Make a name for the exon that indicates its coordinates on the genome and the gene model it came from.
    """
    name='%s:%i-%i'%(seqname,start,end)
    return name

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)
