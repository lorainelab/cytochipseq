# GeneAssign.R

# Create a data frame of gene-peak distances using gene data from a bed or gff file and peak data output form MACS.
geneAssign <- function(genes, peaks, upstream=-3000, downstream=1000){
  # genes: a bed-like file containing at least chr start stop name strand
  # peaks: a bed-like file contianing at least chr start stop name score summit, where score is the fold enrichment calculated by MACS 
  #  and summit is the single numeric position of the peak summit
  # upstream: an integer describing how far downstream (negative numbers equate to upstream) of the start should be considered
  # downstream: an integer describing how far downstream of the TES should be considered.
  #
  # Output will be a data frame with the names: geneName, peakName, fold_enrichment, summit, fromTSS, fromTES
  # geneName, peakName, fold_enrichment and summit are taken from the original input files.
  # fromTSS and fromTES columns indicate how far downstream (negative numbers equate to upstream) the summit of the peak is relative to the start and end  of the gene.
  #
  #set up the data so it has the names and data types that the rest of the function expects
  names(genes)[1:5] = c("chromosome", "chromStart", "chromEnd", "name", "strand")
  names(peaks)[1:6] = c("chromosome", "chromStart", "chromEnd", "name", "fe", "summit")
  #
  genes$chromosome=as.factor(genes$chromosome)
  peaks$chromosome=as.factor(peaks$chromosome)
  Chr=intersect(levels(genes$chromosome), levels(peaks$chromosome))
  #
  #creat data frame around a starter row
  df=data.frame(geneName="geneName", peakName="peakName", 
                foldEnrichment=1, summit=100, fromTSS=-1, fromTES=-1001, 
                stringsAsFactors=F) #this starter row is accounted for after the loops
  #
  #Create a counter for genes without peaks
  genesWithoutPeaks=0
  #
  #Fill in the distances data frame by going through the data one chromosome at a time.
  #For each gene determine which peaks are close enought to be worth considering,
  #and for each of those peaks, determine how far it is from the summit to the gene ends
  for (chr in Chr) {
    print(paste("starting", chr, "..."))
    gc=genes[genes$chromosome==chr,2:5] # genes on a given chromosome, gc
    pc=peaks[peaks$chromosome==chr,2:6] # peaks on a given chromosome, pc
    #
    for (i in 1:nrow(gc)){
      g = gc$name[i]
      strand=as.character(gc$strand[i])
      #
      if(strand=="+"){
        gstart = gc$chromStart[i]
        criteria1 = gstart + upstream
        gend = gc$chromEnd[i]
        criteria2 = gend + downstream
        pcg = pc[pc$chromEnd > criteria1 & pc$chromStart < criteria2,]
        if (nrow(pcg)==0){genesWithoutPeaks=genesWithoutPeaks+1}
        else if (nrow(pcg)>0){
          for (j in 1:nrow(pcg)){
            #calculate distance to summit
            p = pcg$name[j]
            fe = pcg$fe[j]
            summit = pcg$summit[j]
            dist1=summit - gstart #distance1, the position of the summit relative to the start of the gene
            dist2=summit - gend #distance2, the positino of the summit relative to the end of the gene
            df=rbind(df, c(g, p, fe, summit, dist1, dist2))
          }}# end + peak loop and enclosing if{}
      }#end +gene loop
      #below is the -gene loop
      if(strand=="-"){
        gstart = gc$chromEnd[i]
        criteria1 = gstart - upstream
        gend = gc$chromStart[i]
        criteria2 = gend - downstream
        #length = gend-gstart
        pcg = pc[pc$chromEnd > criteria2 & pc$chromStart < criteria1,]
        if (nrow(pcg)==0){genesWithoutPeaks=genesWithoutPeaks+1}
        else if (nrow(pcg)>0){
          for (j in 1:nrow(pcg)){
            #calculate distance to summit
            p = pcg$name[j]
            fe = pcg$fe[j]
            summit = pcg$summit[j]
            dist1=gstart - summit #distance1, the position of the summit relative to the start of the gene
            dist2=gend - summit #distance2, the positino of the summit relative to the end of the gene
            df=rbind(df, c(g, p, fe, summit, dist1, dist2))
          }}# end -peak loop and enclosing if{}
      }#end -gene loop
    }# end entire gene loop
  }#end chr loop
  #
  #account for starter row of df
  df=df[2:nrow(df),]
  #make sure numbers are numbers
  for(i in 3:6){
    df[,i]=as.numeric(df[,i])
  }
  #summary output
  print("Summary of number of peaks assigned to each gene")
  print(summary(as.numeric(table(df$geneName))))
  #
  print(paste("Genes without any peaks:", genesWithoutPeaks))
  #
  print("Summary of number of genes assigned to each peak:")
  print(summary(as.numeric(table(df$peakName))))
  #
  peaksWith=unique(df$peakName)
  peaksWithout = which( ! (peaks$name %in% peaksWith))
  peaksWithout = peaks[peaksWithout,]
  n=nrow(peaksWithout)
  print(paste("Number of unnassigned peaks:", n))
  if (n > 0) {
    print("Suammry of foldEnrichments of unassigned peaks:")
    print(summary(as.numeric(peaksWithout$fe)))
  }
  return(df)
}#end geneAssign function



# this function takes a data frame as input and returns that data frame minus the peaks that were over-assigned.
removeOverAssignedPeaks <- function(pg, m=4, bed=NULL, fname=NULL){
  # pg = peak-gene pairing, data frame with a column for the peaks (called "peakName") and a column for the genes (called "geneName").
  # m = max number of genes that a peak can be assigned to.
  # bed = bed file describing peaks.  doesn't really need to be a bed file, but it must have rownames and a name column that match the peak names in pg
  # fname = file name to write out the unnassigend peaks
  t=table(pg$peakName)
  t=t[order(t, decreasing=T)]
  head(t)
  table(t)
  # over-assigned peaks
  op=names(t)[t>m]
  if (!is.null(fname)) { 
    if (!is.null(bed)){
      op=bed[(b2.c$name %in% op),]
      write.table(op, file=fname, quote=F, sep='\t', row.names=F, col.names=T)
      summary(op$score)
      op=op$name
    }else{
    write.table(op, file=fname, quote=F, sep='\n', row.names=F, col.names=T)
  } 
  }
  # remove them from peak-gene table
  w=which(! pg$peakName %in% op)
  pg=pg[w,]
  print(paste("We tossed out the", length(op), "peaks that were assigned to more than", m, "genes."))
  return(pg)
}



# Create a data frame of gene-peak distances using gene data from a bed or gff file and peak data output form MACS.
geneAssignFast <- function(genes, peaks, upstream=-3000, downstream=1000){
  # genes: a bed-like file containing chromosome chromStart chromEnd name strand, in that order.
  # peaks: a bed-like file contianing chromosome chromStart chromEnd name fe summit, 
  #  where fe is the fold enrichment calculated by MACS 
  #  and summit is the single numeric position of the peak summit
  # upstream: an integer describing how far downstream (negative numbers equate to upstream) of the start should be considered
  # downstream: an integer describing how far downstream of the TES should be considered.
  #
  # Output will be a data frame with the names: geneName, peakName, fold_enrichment, summit, fromTSS, fromTES
  # geneName, peakName, fold_enrichment and summit are taken from the original input files.
  # fromTSS and fromTES columns indicate how far downstream (negative numbers equate to upstream) the summit of the peak is relative to the start and end  of the gene.
  #
  #set up the data so it has the names and data types that the rest of the function expects
  names(genes)[1:5] = c("chromosome", "chromStart", "chromEnd", "name", "strand")
  names(peaks)[1:6] = c("chromosome", "chromStart", "chromEnd", "name", "foldEnrichment", "summit")
  #
  neg = which(genes$strand=='-')
  genes$rangeStart = genes$chromStart + upstream
  genes$rangeStart[neg] = genes$chromStart[neg] - downstream
  genes$rangeEnd = genes$chromEnd + downstream
  genes$rangeEnd[neg] = genes$chromEnd[neg] - upstream
  genes$rangeStart[genes$rangeStart < 0] = 0
  #
  # Write a couple of temp files
  options(scipen=999)
  write.table(genes[,c("chromosome", "rangeStart", "rangeEnd", "name")], 
              ".tempGenes.bed", sep='\t', col.names=F, row.names=F, quote=F)
  write.table(peaks[1:4], ".tempPeaks.bed", sep='\t', col.names=F, row.names=F, quote=F)
  #
  # Run bedtools interesectBed
  cmd="intersectBed -a .tempGenes.bed -b .tempPeaks.bed -wa -wb | cut -f 4,8"
  hits = read.table(pipe(cmd), sep='\t')
  names(hits) = c("geneName", "peakName")
  system("rm .tempGenes.bed")
  system("rm .tempPeaks.bed")
  #
  # merge the association with the peak and gene information
  hits=merge(hits, genes, by.x="geneName", by.y="name")
  hits=merge(hits, peaks[,c("name", "foldEnrichment", "summit")], by.x="peakName", by.y="name")
  #
  # determine distance from TSS for each pair
  neg=which(hits$strand=='-')
  hits$fromTSS = hits$summit - hits$chromStart
  hits$fromTSS[neg] = hits$chromEnd[neg] - hits$summit[neg]
  #
  # determine distance from TES for each pair
  hits$fromTES = hits$summit - hits$chromEnd
  hits$fromTES[neg] = hits$chromStart[neg] - hits$summit[neg]
  #
  hits=hits[,c("geneName", "peakName", "foldEnrichment", "summit", "strand", "fromTSS", "fromTES")]
  # 
  #
  #summary output
  print("Summary of number of peaks assigned to each gene")
  print(summary(as.numeric(table(hits$geneName))))
  #
  print(paste("Genes without any peaks:", sum(!(genes$name %in% hits$geneName))))
  #
  print("Summary of number of genes assigned to each peak:")
  print(summary(as.numeric(table(hits$peakName))))
  #
  peaksWithout = which( ! (peaks$name %in% hits$peakName))
  peaksWithout = peaks[peaksWithout,]
  n=nrow(peaksWithout)
  print(paste("Number of unnassigned peaks:", n))
  if (n > 0) {
    print("Suammry of foldEnrichments of unassigned peaks:")
    print(summary(as.numeric(peaksWithout$fe)))
  }
  return(hits)
}#end geneAssignFast function



# Split this into three parts positive strand, negative strand 
# and cases where the peak is in the gene.
# We will take up to one entry for each peak for each strand and 
# keep all records of the peak being in a gene.
limmit1GenePerDirection = function(psgAll){
  psgAll$strand = as.character(psgAll$strand)
  psgAll$strand[psgAll$fromTSS > 0 & psgAll$fromTES <= 0] = "inGene"
  psgAll = psgAll[order(psgAll$fromTES, decreasing=T),]
  PSG = split(psgAll, f=psgAll$strand)
  PSG[['-']] = PSG[['-']][!duplicated(PSG[['-']]$peakName),]
  PSG[['+']] = PSG[['+']][!duplicated(PSG[['+']]$peakName),]
  psg = rbind(PSG[['inGene']], PSG[['-']], PSG[['+']])
  return(psg)
}