---
title: "Associate a gene with each BApeak"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

```{r include=FALSE}
library(knitr)
knitr::opts_chunk$set(fig.path = 'figure/')
knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",")
})
```

# Introduction

We need to associate genes with the peaks. 

Question:

 * For each BApeak, what is the closest downstream gene?
 
**Requres bedtools interesectBed**

# Method

## Read data

### BApeaks

Read in the BApeaks, peaks shared across all three BA-treated samples.
```{r read BApeaks}
source("../src/ProjectVariables.R")
source("../src/BedFiles.R")

fname.peaks="../SummitRegions/results/BASharedSuperSummits.bed.gz"
peaks <- read.bed(fname.peaks)
peaks$fold_enrichment = NA #The geneAssignFast() function expects this column.
peaks$thickStart = with(peaks, round((chromStart + chromEnd)/2,0))
row.names(peaks) = peaks$name
```


### Genes

Read in genes.
```{r read genes file}
T10 = read.bed("../ExternalDataSets/TAIR10.bed.gz")
T10 = T10[,c(1:4,6,13,14)]
```

Identify the most distal start/stop position among models for the same gene.
```{r determine gene range}
T10$name = truncateTranscriptID(T10$name) # extract gene identifiers
T10=getGeneRange(T10)
```

Set up this data frame with one row for each gene, using the most distal of start and stop locations as its only start and stop.
```{r}
T10 = data.frame(chromosome=T10$chromosome, chromStart=T10$rangeStart, chromEnd=T10$rangeEnd, name=T10$name, strand=T10$strand, symbol=T10$ID, description=T10$description)
T10 = T10[!duplicated(T10$name),]
T10$name = as.character(T10$name)
```

## Connect Peaks to Single closest Downstream Gene

Match up peaks to genes.  geneAssignFast() will match up a peak to every gene within the specified range.  Start with that and then pick only the closest gene(s) to each peak.
```{r}
source("src/GeneAssign.R")
# pair up peaks with genes
# This function requires bedtools interesectBed.  If this is unavailable, use geneAssign().
upstream=20000
psgAll <- geneAssignFast(genes=T10, peaks=peaks[,c("chromosome", "chromStart", "chromEnd", "name", "fold_enrichment", "thickStart")], upstream=-upstream, downstream=0)
psgAll = psgAll[psgAll$fromTES <= 0,]
```

Limit this set to the nearest downstream gene.  "Downstream" is meaningfull for genes, but peaks have no direction, so we'll allow a peak to be associated with one gene (the closest downstream gene) on each strand. Additionally, the peak may be associated with any gene it is internal to.  So each peak will be associated with 2 or more genes.
```{r}
psg = limmit1GenePerDirection(psgAll)
```

Visualize: Across all associations, how far is the summit from the TSS?
```{r}
source("src/showSummitLocations.R")
showSummitToTSS(psgAll, psg, upstream=upstream, downstream = 1000)

maxDist = 1500
abline(v=-maxDist, col=gray(.3), lty=2)
```

Peaks were matched with `r length(unique(psg$geneName))` unique genes.  Some peaks are very far away from the closest gene. Many of these may also have a much closer gene on the other strand.  Apply a distance restriction of `r maxDist` bp.

## Limit Peak to Gene Distance

We'll use a maxiumum distance of `r maxDist` which is shown in the plot above as a gray dashed line.
```{r fig.width=4, fig.height=4}
psg = psg[psg$fromTSS > -maxDist,]
showSummitToTSS(psgAll[psgAll$fromTSS > -maxDist,], psg, 
                upstream=maxDist, downstream = 1000, 
                xaxisMarks=500)
```

## Write file

Add extra peak info.
```{r}
samples = getBAsampleNames()
# remove the 'foldEnrichment' colum
psg=psg[-3]
# get the fold enrichment for each peak
cmd=paste0('../src/matchPeakScores.py -s ', fname.peaks, ' -p ../PeakProcessing/results/peaks.tsv.gz')
peakFE = read.delim(pipe(cmd),header=T,as.is=T,sep='\t')
w=which(names(peakFE) %in% samples)
names(peakFE)[w]=paste0(names(peakFE)[w], "_FE")
# add relevant fold enrichment scores
psg = merge(psg, peakFE[,c("name", names(peakFE)[w])], 
            by.x="peakName", by.y="name", all.x=T)
```

Add extra gene info.
```{r}
psg = merge(psg, T10[,c("name", "symbol", "description")], 
            by.x="geneName", by.y="name", all.x=T)
```

Sort by foldEnrichment.
```{r}
psg$meanFE = apply(psg[grep("FE", names(psg))],1,mean)
psg = psg[order(psg$meanFE, decreasing = T),]
```


### peak to gene associations
Write a file for relating each peak to its nearest gene.
```{r}
fname = paste0("results/ConsensusPeaks_ClosestGene_limit", maxDist,"bp.txt")
w=which(names(psg)=="description")
write.table(psg, file=fname, sep='\t', row.names=F, quote=w) #use quotes on description column
system(paste('gzip -f', fname))
fname
```

### unique gene list
Write a file with just the genes, in a non-redundant list.  Remove the peak information, but create a new column that gives a camma separated list of the fromTSS values for the one or more peaks associated with that gene.
```{r echo=FALSE}
#put rows in order by fromTSS
psgGenes=psg[order(psg$fromTSS),]
psgGenes$geneName=as.character(psgGenes$geneName)
#make a list of the ordered fromTSS values for each gene
sp=split(psgGenes$fromTSS, f=psgGenes$geneName)
#remove duplicate entries for each gene
psgGenes=psgGenes[!duplicated(psgGenes$geneName),]
#make a column of the list elements as a camma separated list.
fromtss=sapply(sp, paste, sep=",", collapse=",")
psgGenes$fromTSSlist=fromtss[as.character(psgGenes$geneName)]
#remove columns of peak info
psgGenes=psgGenes[,c("geneName", "symbol", "fromTSSlist", "description")]
#order by gene
psgGenes=psgGenes[order(psgGenes$geneName),]
#write file
fname = paste0("results/ConsensusPeaks_ClosestGene_limit", maxDist,"bp-GeneList.txt")
w=which(names(psgGenes)=="description")
write.table(psgGenes, file=fname, sep='\t', row.names=F, quote=w) #use quotes on description column
system(paste("gzip -f", fname))
```
The list of `r length(unique(psgGenes$geneName))` unique genes is saved as 
```{r echo=FALSE}
fname
```


# Conclusion
Using these `r nrow(peaks)` BApeaks, we linked `r length(unique(psg$peakName))` of them to at least one gene. We found putative binding for `r length(unique(psg$geneName))` unique genes, ~`r round(100*length(unique(psg$geneName))/nrow(T10), 0)`% of the `r nrow(T10)` genes in the genome. 

**Session Info**
```{r}
sessionInfo()
```
**Functions** that are used here but defined elsewhere:
```{r}
read.bed
getGeneRange
truncateTranscriptID
write.bed
geneAssignFast
limmit1GenePerDirection
showSummitToTSS
```




