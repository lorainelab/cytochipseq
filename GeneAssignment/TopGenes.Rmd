Genes nearest best-scoring peaks
================

Introduction
------------

This Markdown answers:

* Which genes are near best-scoring peaks?

Analysis
--------


### Read peak to gene assignment file

```{r}
fname='results/ClosestGene_1500bp_AllPeaksQ10.txt' # not sure if this right file
d=read.delim(fname,header=T,sep='\t')
original.d=d
```

### Get score from the top scoring peak next to genes

```{r}
o=order(d$neg.log10.qvalue,decreasing=T)
d=d[o,]
d=d[!duplicated(d$geneName),c('geneName','neg.log10.qvalue',
                              'foldEnrichment','description')]
d$neg.log10.qvalue=round(d$neg.log10.qvalue,1)
d$foldEnrichment=round(d$foldEnrichment,1)
names(d)=c('AGI','Q','FE','descr')
d=d[1:100,]
```

### Write file

```{r}
fname='results/top100.tsv'
write.table(d,file=fname,quote=F,row.names=F,sep='\t')
```

Genes with top-scoring peaks included:

* Several type A RRs, a cytokin oxidase, and many other genes up-regulated by cytokinin
* Genes involved in hormone responses, including two SAUR-like auxin-responsive protein family genes and JAZ6

About JAZ6:

>>AZ6 transcript levels rise in response to a jasmonate stimulus and a GFP:JAZ6 fusion protein localizes to the nucleus. Application of jasmonate methyl ester to Arabidopsis roots reduces the levels of a JAZ6:GUS fusion protein, presumably by stimulating ubiquitin-proteasome-mediated degradation.

Conclusions
-----------

Genes that are near high-scoring peaks include many genes that are up-regulated by cytokinin treatment. 


