---
title: "Summit locations relative to genes and making figure 6C"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction

### Questions
Figure 6A looks at peak overlap. Figure 6B looks at locations of the super summits that are shared by all BA samples (which is essentially a subset of the three-way peak overlap, there are only 19 shared peaks that had multiple BAsummits).  Here, we are looking at the BAsummits again, but rather than counting how many fall on one side or the other of the gene boundary, here we want to see how far they fall from the gene boundary. 

We expect that the promoter, the region just upstream of the gene, will have the most binding.  
Our questions are:

* How far are the summits from the transcription start sites?
* Are they typically in the area that we would consider to the be promoter?

We'd also like to know:

* Is whatever pattern we see consistent across samples?
* Is the pattern different between genes that are upregulated, or downregulated?

```{r include=FALSE}
knitr::opts_chunk$set(fig.path = 'figuresLikeFig6C/')
```

### Input Data
```{r}
source("../src/ProjectVariables.R")
peakDistLimit = getPeakDistLimit()
upstream=peakDistLimit["upstream"]
minPeakQ = getMinPeakQ()
fname = paste0("results/peak2gene_", upstream, "bp_minQ", minPeakQ,".tsv.gz")
PG <- read.table(fname, header=T, sep='\t', quote='"', as.is=T)
PG = split(PG, f=PG$sample)

files = dir("results/")[grep("GeneAssignments_", dir("results/"))]
n = length(PG)
for (i in 1:length(files)){
  PG[[n+i]] = read.table(file=paste0("results/",files[i]), sep='\t', header=T, quote='"')
  names(PG)[n+i] = gsub("GeneAssignments_", "", files[i])
  names(PG)[n+i] = gsub(".txt", "", names(PG)[n+i])
}
names(PG)
```

To see if there is a relationship between binding location and regulation, we'll make subsets of the summits group looking only at those that are near up/down/non-regulated genes. 

Peaks were associated with genes in GeneAssignment/ConsensusPeaksToClosestGenes.Rmd. Genes associated with peaks were categorized as being up- or down-regulated by cytokinin in CompareToExpression/ExpressionSummary.Rmd. Use the results of that markdown to create sets of peaks that up- or down-regulated based on the genes they are near.

```{r echo=FALSE}
# geneExpr = read.delim("../ExternalDataSets/Cytokinin.tsv.gz", sep='\t', quote="", header=T, as.is=T)
# upReg = geneExpr[geneExpr$fdr<10e-3 & geneExpr$logFC > 1,"locus"]
# dnReg = geneExpr[geneExpr$fdr<10e-3 & geneExpr$logFC < -1,"locus"]
# nonReg = geneExpr[geneExpr$fdr>.5 & abs(geneExpr$logFC) < .5,"locus"]
# 
# #summits = rbind(PG$Exp1_Rep1, PG$Exp1_Rep2, PG$Exp2_BA)
# summits = PG$BAsummits
# PG$upRegSummits = summits[(summits$geneName %in% upReg),]
# PG$dnRegSummits = summits[(summits$geneName %in% dnReg),]
# PG$nonRegSummits = summits[(summits$geneName %in% nonReg),]
# names(PG)
# 
```


```{r}
geneExpr=read.delim("../CompareToExpression/results/GeneList.txt.gz", as.is=T)[1:3]
```
This set includes `r nrow(geneExpr)` rows, `r length(unique(geneExpr$geneName))` unique genes.

```{r}
anyDE=unique(geneExpr$geneName[which(!is.na(geneExpr$direction))]) # may not use this one
upReg=unique(geneExpr$geneName[which(geneExpr$direction > 0)])
dnReg=unique(geneExpr$geneName[which(geneExpr$direction < 0)])
nonReg = setdiff(unique(PG$BAsummits$geneName), anyDE)
```

```{r echo=FALSE, fig.width=4, fig.height=3}
x = c(downReg = length(intersect(dnReg, PG$BAsummits$geneName)),
      upReg = length(intersect(upReg, PG$BAsummits$geneName)),
      mixed = length(intersect(setdiff(anyDE, union(upReg, dnReg)), PG$BAsummits$geneName)),
      nonReg = length(nonReg) )
cols = c("pink", "lightblue","orange", gray(.7))
x
pie(x, init.angle = 180, main="Genes near BAsummits", col = cols)
```
The majority of the `r length(unique(PG$BAsummits$geneName))` genes near BAsummits were not found to be DE in any of the expression studies we considered.

Define subgroups of the BAsummits (consensus peaks) based on these sets of genes.
```{r}
summits = PG$BAsummits
PG$upRegSummits = summits[(summits$geneName %in% upReg),]
PG$dnRegSummits = summits[(summits$geneName %in% dnReg),]
PG$nonRegSummits = summits[(summits$geneName %in% nonReg),]
names(PG)
```


# Basic plots of distances to genes--this section includes many plots

If we consider all BA sample summits individually, we can get bigger numbers, but we will have greater confidence in watever we see if we only consider the BAsummits, the sites that have a summit from all three BA samples.

Use these colors.
```{r}
# For the larger set
lightCol = grey(0.7)
# For the subset after removing duplicates
darkCol = grey(0.4)
```

### Summits Relative to Transcription Start Site
```{r}
# TSS
for (I in names(PG)){
  dist = PG[[I]]
  #  dist = dist[,c("geneName", "fromTSS")]
  dist = dist[order(abs(dist$fromTSS)),]
  dist = dist[dist$fromTSS<3000 & dist$fromTSS>-3000,]
  hist(dist$fromTSS, breaks=50, add=F, col=lightCol, xpd=F,
       main=paste("Distance between", I, "summits and TSS"),
       xlab="Distance to TSS")
  ymax=par("usr")[4]
  xmax=par("usr")[2]
  polygon(x=c(0,0,xmax,xmax), y=c(0.9*ymax,ymax,ymax,0.9*ymax), col="#6BAED6")
  hist(dist$fromTSS, breaks=50, add=T, col=lightCol, xpd=F,
       main=paste("Distance between", I, "summits and TSS"),
       xlab="Distance to TSS")
  mtext("negative values indicate that the peak is upstream of the gene", side=1, line=2)
  legend(x="topleft", legend=c("all summits", "closest summit"), 
         col=c(lightCol,darkCol), pch=15, bty='n')
  
  peakOrSummit = c("peakName", "SuperSummit")[c("peakName", "SuperSummit") %in% names(dist)]
  dist = dist[!duplicated(dist[,peakOrSummit]),]
  hist(dist$fromTSS, breaks=50, add=T, col=darkCol)
  
  abline(v=0, lwd=2)
  text(x=.5*xmax, y=.95*ymax, labels="gene", xpd=T)
  mtext("TSS", side=3, line=0)
  }
```

### Summits Relative to Transcription End Site
```{r}
# TES
for (I in names(PG)){
  dist = PG[[I]]
  dist = dist[order(abs(dist$fromTES)),]
  dist = dist[dist$fromTES<1000 & dist$fromTES>-1000,]
  hist(dist$fromTES, breaks=50, add=F, col=lightCol,
       main=paste("Distance between", I, "summits and TES"),
       xlab="Distance to TES")
  ymax=par("usr")[4]
  xmin=par("usr")[1]
  polygon(x=c(0,0,xmin,xmin), y=c(0.9*ymax,ymax,ymax,0.9*ymax), col="#6BAED6")
  hist(dist$fromTES, breaks=50, add=T, col=lightCol,
       main=paste("Distance between", I, "summits and TES"),
       xlab="Distance to TES")
  mtext("positive values indicate that the peak is downstream of the gene", side=1, line=2)
#     legend(x="top", legend=c("all summits", "closest summit to each TES"), 
#            col=c(lightCol,darkCol), pch=15, bty='n')
  
  peakOrSummit = c("peakName", "SuperSummit")[c("peakName", "SuperSummit") %in% names(dist)]
  dist = dist[!duplicated(dist[,peakOrSummit]),]
  hist(dist$fromTES, breaks=50, add=T, col=darkCol)
  
  abline(v=0, lwd=2)
  text(x=.5*xmin, y=.95*ymax, labels="gene", xpd=T)
  mtext("TES", side=3, line=0)
  }
```


# Figure 6C
For this, just look at the BA super summits.

### Simple figure to meet basic requirements

Make a simple figure to just meet the basic criteria set for figure 6C on the basecamp toDo page.
```{r spec}
dist = PG[["BAsummits"]]
dist = dist[order(abs(dist$fromTSS)),]
dist = dist[dist$fromTSS<0 & dist$fromTSS>-3000,]
dist = dist[!duplicated(dist$SuperSummit),]
hist(dist$fromTSS, breaks=50, add=F, col=darkCol, xpd=F,
     main="Distance between BA summits and TSS",
     xlab="Distance to TSS")
ymax=par("usr")[4]
xmax=par("usr")[2]
mtext("negative values indicate that the peak is upstream of the gene", side=1, line=2)
```

This first figure is the closest match to the spec for Figure 6C.

There is much more information that could be brought together into a single more meaningful figure, by combining all of what we know about where summits fall upstream and downstream of the gene and we can draw an in-gene region in between them.

### Details for better figure

For the in-gene region, we'll draw a histogram just like we will for the upstream and downstream regions, but here the x-axis will be on a scale that is relative to the gene.  This will demonstrate that when summits land in the gene, they are most often at the very beginning of the gene, and the hits through the rest of the gene occur at a lower frequency than hits outside of a gene.  For how histograms work, the bars in this in-gene region can be much taller or much shorter than the upstream or downstream bars based on how wide we make this.  A bar may have the same width in the diagram, but one bar in the upstream region equates to some n bases while a bar in the in-gene region equates to some n% portion of genes.  If the we make the in-gene region get only a few bars, they'll all be large portions of the area and all really tall.  If we make the in-gene region really wide with a lot of bars then they'll all be shorter.  So if we want to make the bars the same width on all parts of the diagram, and we want the three parts to be at all comparable, then we need to make the size of the in-gene the same size as the genes it references.  They're not all the same size, so we'll approximate by taking the mean of their sizes.
```{r}
dist = PG[["BAsummits"]]
inGene = dist$fromTSS>0 & dist$fromTES<0
distIG = dist[inGene,]
lengths = distIG$fromTSS-distIG$fromTES
geneModel=round(mean(lengths), -2)
numGenes = length(unique(distIG$geneName))
```

This figure will be spread across three parts: upstream, in gene, and downstream.
The upstream portion will be just like what we did above.  The downstream portion will be like whats above but the numbers will be increased by about `r geneModel`. The increase is to make room for a gap in the middle where we can plot the in-gene hits. Within the in-gene, the scale will be in terms of proportion of the gene where 0 is the very beginning and `r geneModel` is the end.

Use a custom function to make this figure.
```{r}
source("src/showSummitLocations.R")
```



### Figure for panel C in Fig.6

```{r}
fname="figure6C/Fig6C-6.7in.png"
showSummitLocations(dist = PG[["BAsummits"]],
                    fname=fname,
                    geneModelSize = geneModel,
                    main="BA Summit Locations", detailRepeats=T)
```


Show image from file.

<img src="`r fname`" alt="oops, was the file `r fname` created?" style="width:6.7in;height:3.4in">

### supplemental figure - up-regulated genes

```{r}
fname="figure6C/SummitsNearUpRegGenes-6.7in.png"
showSummitLocations(dist = PG[["upRegSummits"]],
                    fname=fname,
                    geneModelSize = geneModel,
                    main="BA Summit Locations Near Up-Regulated Genes")
```

Show image from file.

<img src="`r fname`" alt="oops, was the file `r fname` created?" style="width:6.7in;height:3.4in">

### supplemental figure - down-regulated genes

```{r}
fname="figure6C/SummitsNearDownRegGenes-6.7in.png"
showSummitLocations(dist = PG[["dnRegSummits"]],
                    fname=fname,
                    geneModelSize = geneModel,
                    main="BA Summit Locations Near Down-Regulated Genes")
```

Show image from file.

<img src="`r fname`" alt="oops, was the file `r fname` created?" style="width:6.7in;height:3.4in">

### supplemental figure - non-DE genes

```{r}
fname="figure6C/SummitsNearNonDEGenes-6.7in.png"
showSummitLocations(dist = PG[["nonRegSummits"]],
                    fname=fname,
                    geneModelSize = geneModel,
                    main="BA Summit Locations Near Non-DE Genes")
```

Show image from file.

<img src="`r fname`" alt="oops, was the file `r fname` created?" style="width:6.7in;height:3.4in">

# Conclusion
### Patterns and pattern consistency

The area immediately surrounding the TSS (within 500 bases on either side) has more hits than area's further away.  This seems to be the case in all samples, even the mock.

For the BAsummits (the super summits that had summit from all three treated samples), there are more hits upstream of the TSS (in the promoter) than there are downstream of the TSS (in the gene).  This pattern is much less apparent when we look at AllSites (all regions that had a summit from any sample).  And it is true in all of the BA samples but does not seem to be true for the mock treated sample.

Interestingly, the TES graphs are completely the opposite. There seems to be a definitive dip in the number of hits immediately surrounding the TES, and the pattern is most apparent in AllSites and in the mock sample.  The ratio of hits inside the gene vs outside of the gene is inconsistent across samples.

### Hits within genes
Among BAsupper summits that hit inside the gene, most hit the very beginning of the gene.  This could be the first exon or it could the area upstream of other variants whose TSS is slightly later than the most distal TSS. I wonder how the hits-to-introns vs hits-to-exons ratio would look if we removed hits to the first exon and intron, or even just removed hits that are upstream of any TSS for any of the gene's variants.  Without removing this front-of-gene preference, the the relative number of hits to exons and introns is the same. Its very possible that removing it would make no noticible difference.

### Regarding gene expression
In terms of gene expression, I think we have a large enough number of genes in the up-reg or down-reg categories (`r nrow(PG$upRegSummits)` and `r nrow(PG$dnRegSummits)` gene-summit pairs) to see basic patterns, if not very clear patterns.  However it is noteworthy that of the `r length(nonReg)` genes that were not found to be regulated by cytokinin, `r length(unique(PG$nonRegSummits$geneName))` of them (`r round(100*length(unique(PG$nonRegSummits$geneName))/length(nonReg),0)`%) had at least one summit somewhere around them, and `r length(unique(PG$nonRegSummits[(abs(PG$nonRegSummits$fromTSS)<300),"geneName"]))` (`r round(100*length(unique(PG$nonRegSummits[(abs(PG$nonRegSummits$fromTSS)<300),"geneName"]))/length(nonReg), 0)`%) had a summit within 300 bases of the TSS.  These conflict with the hypothesis that the near-TSS pattern we see in summits is due to ARR10 binding to the promoters of genes and inducing transcription.  But this is not a large percentage of the non-Reg group, so I think we can write it off for now, see Limitations.  (When I considered all summits from any BA sample, 19% of non-Reg genes had a summit <300 bp from the TSS.)

It seems pretty clear that the genes with BAsummits immediately upstream of the TSS are not exclusively cytokinin-regulated genes.  We probably a lot of peaks from other proteins that also bind to the promoter region of the their respective targets.

# Limitations
### Regarding the choice to show only the closes summit
It is noteworthy that outside of that tight range of increased frequency, the numbers of summits are pretty stable across a wide range of distance from the TSS.  The appearance of a gradual slope of lower frequency as you move away from the TSS is only seen when we only consider the single closest summit or closest gene, which artificially produces the effect.

### Regarding gene range
In looking at these figures it is important to remember that genes come in a wide range of sizes, and the side of the diagram marked as "gene" is just there to tell you which side of the TSS/TES the gene region is on.  Some genes may end before the end of the diagram.
In the main figure 6C, we have approximated the gene size using the mean size of genes involved, see discussion above.

### Regarding gene expression
What we have used here to determien up- and down- regulated genes is not perfect.  Its based on one experiment. We don't know what portion of these genes have the regulation pattern that they have as a result of type-B regulators.


**Housekeeping**
```{r housekeeping}
sessionInfo()
```