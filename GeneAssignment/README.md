Peak to Gene Assignment
=========================

Connecting peaks to genes.

* * *

### Exploratory-Peak-ClosestGene.Rmd

Exploratory.  This markdown associates each individual peak (from each of the four samples) with the nearest downstream gene, and poses several questions about the relationships between peaks and genes and between samples.

* * *

### Exploratory-Peak-Gene_assignment.Rmd

Exploratory.  This markdown associates individual peaks (from each of the four samples) to one or more genes (or none) based on a window around the gene. The markdown poses several questions about the relationships between peaks and genes and between samples.

* * *

### MakeFigure6C.Rmd

This markdown depends on the peak to gene associations.  It makes a figure showing the distance between TSS and sumit.

* * *

### MakeFigure6D.Rmd

This markdown depends on the peak to gene associations.  It looks at the scores of all peaks in one sample, the scores of the consensus peaks (a subset of the later) and it highlights the peaks that were linked to type-A response regulators, showing that known targets have high scores.

* * *

### TopGenes.Rmd

Selects the genes associated with the 100 top scoring peaks.

* * *

#### figure6C

Images intended to be used for a specific figure in the manuscript.

* * *

#### figure6D

Images intended to be used for a specific figure in the manuscript.

* * *

#### IGBimages

Images generated in IGB, and sometimes annotated in powerpoint. These are helpful in describing how genes are being associated with peaks.

* * *

#### results

Files with genes associated with peaks using various methods.