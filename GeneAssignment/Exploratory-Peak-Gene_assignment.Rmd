---
title: "Associate genes with the peak and super summit regions"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

```{r include=FALSE}
knitr::opts_chunk$set(fig.path = 'figure/')
source("../src/ProjectVariables.R")
minPeakQ = getMinPeakQ()
```

# Introduction

We need to associate genes with the peaks. 

* Which genes are near which peaks?
* How far apart are genes and peaks?
* Are there many peaks that are not near any genes?

We have defined area's of interest based on summits from all samples (AllSites) and summit regions that are common to all BA samples (BAsharedSummits). We need to associate gene with these, too.
In this mark down, we will determine the distance between the peak summit and the gene TSS for every peak-gene combination where the peak is no more than `r getPeakDistLimit()["upstream"]`bp upstream of the gene and no more than `r getPeakDistLimit()["downstream"]`bp downstream of the gene.

**Requires bedtools interesectBed**

# Method

The image below shows a window around each gene.  Peaks that overlap the window are assigned to that gene.  A peak may be assigned to many genes and a gene may be assigned to many peaks.  For genes with more than one transcript, the window is based on the most distal TSS and TES.

<img src="IGBimages/GeneAssignment.png" alt="oops, is the file IGBimages/GeneAssignment.png still there?">

## Read and arrange input files

### Peaks

Read in the MACS2 output data found in the peaks.xls file. Store the data as a list with an element for each sample.
```{r read sample peaks}
source("../src/ProjectVariables.R")
minPeakQ = getMinPeakQ()

macsAll = read.table("../PeakProcessing/results/peaks.tsv.gz", sep='\t', header=T, quote="", as.is=T)
macsAll = macsAll[macsAll$neg.log10.qvalue>=minPeakQ,]

peakColNames = names(macsAll) #this may be used to pick out the columns from the original file
macs=split(macsAll, f=macsAll$sample)
#
samples <- getSampleNames() #see projectVariables
macs = macs[samples] # make sure this list is in the right order and matches the sample names
```
We are only considering peaks whose Q value is at least `r minPeakQ`.

This code was written for comparing bed files. Add columns to make this object work with the bed-oriented script.
```{r}
for (I in samples){
  macs[[I]]$chromosome = macs[[I]]$chr
  macs[[I]]$chromStart = macs[[I]]$start - 1
  macs[[I]]$chromEnd = macs[[I]]$end
  macs[[I]]$score = macs[[I]]$fold_enrichment * sqrt(macs[[I]]$length)
  macs[[I]]$strand = '+'
  macs[[I]]$thickStart = macs[[I]]$abs_summit - 1
  macs[[I]]$thickEnd = macs[[I]]$abs_summit
}
bednames=c("chromosome", "chromStart", "chromEnd", "name", "score", "strand", "thickStart", "thickEnd") #this may be used to pick out the columns to use in a bed file.
```

### Summit Regions

Read in the bed file for the definition of AllSites (merged summits from all samples) and let this go through the same steps to be assigned to genes. Be liberal with this assignment, it can be parsed down later as needed.
```{r read allSites}
source("../src/BedFiles.R")

macs$AllSites <- read.bed("../SummitRegions/results/SuperSummitRegions.bed")
macs$AllSites$fold_enrichment = NA
AS.length = macs$AllSites$chromEnd - macs$AllSites$chromStart
macs$AllSites$thickStart = macs$AllSites$chromStart + round(AS.length/2, digits=0)
macs$AllSites$thickEnd = macs$AllSites$thickStart + 1
row.names(macs$AllSites) = macs$AllSites$name
#
macs$BAsummits <- read.bed("../SummitRegions/results/BASharedSuperSummits.bed.gz")
macs$BAsummits$fold_enrichment = NA
macs$BAsummits$thickStart = with(macs$BAsummits, round((chromStart + chromEnd)/2,0))
row.names(macs$BAsummits) = macs$BAsummits$name
```

### Genes

Read in genes
```{r read genes file}
T10 = read.bed("../ExternalDataSets/TAIR10.bed.gz")
T10 = T10[,c(1:4,6,13,14)]
```

Identify the most distal start/stop position among models for the same gene.
```{r determine gene range}
T10$name = truncateTranscriptID(T10$name) # extract gene identifiers
T10=getGeneRange(T10)
```

Set up this data frame with one row for each gene, using the most distal of start and stop locations as its only start and stop.
```{r}
T10 = data.frame(chromosome=T10$chromosome, chromStart=T10$rangeStart, chromEnd=T10$rangeEnd, name=T10$name, strand=T10$strand, symbol=T10$ID, description=T10$description)
T10 = T10[!duplicated(T10$name),]
T10$name = as.character(T10$name)
```

## Connect Peaks to Genes

Get the distance between each gene and each peak or merged summit (AllSites). Add on the negLogPval for each peak.
```{r peak to gene table}
peakDistLimit = getPeakDistLimit()
source("src/GeneAssign.R")

pg = as.list(rep(NA, length(macs)))
names(pg) = names(macs)

upstream=peakDistLimit["upstream"]
downstream = peakDistLimit["downstream"]
for (I in names(pg)){
  print(I)
  # pair up peaks with genes
  # This function requires bedtools interesectBed.  If this is unavailable, use geneAssign().
  pg[[I]] <- geneAssignFast(genes=T10, peaks=macs[[I]][,c("chromosome", "chromStart", "chromEnd", "name", "fold_enrichment", "thickStart")], upstream=-upstream, downstream=downstream)
  # add on more gene info
  pg[[I]] = merge(pg[[I]], T10[,c("name", "symbol", "description")], by.x="geneName", by.y="name", all.x=T)
  }

for (I in samples){
  # add on more peak info
  pg[[I]] = merge(pg[[I]], macs[[I]][,c("name", "neg.log10.qvalue", "neg.log10.pvalue", "length")], 
                  by.x="peakName", by.y="name")
  }
names(pg[[1]])
```
Each gene is connected to all peaks that are within `r upstream`bp upstream of the TSS and `r downstream`bp downstream of the TES. TSS=Transcription Start Site, TES=Transcription End Site.

# Comments

Comment: How many peaks were assigned to exactly 1 gene?
```{r comments on single gene assignment}
for (I in names(pg)){
t = table(pg[[I]]$peakName)
t2=table(t)
total = nrow(macs[[I]])
percentage = round(t2["1"]*100/nrow(macs[[I]]), digits=0)
print(paste("Of the", total, "regions in", paste0(I,","), t2["1"], paste0("(", percentage,"%)"), "were associated with exactly one gene."))
}
```

Comment: How many peaks were assigned to more than `r getMaxGenesPerPeak()` genes? 
```{r comments on overassignment}
maxGenesPerPeak = getMaxGenesPerPeak()
for (I in names(pg)){
  t = table(pg[[I]]$peakName)
  t2=table(t)
  total = nrow(macs[[I]])
  if (length(t2 > maxGenesPerPeak)) {overAssigned <- sum(t2[(maxGenesPerPeak+1):length(t2)])}
  else {overAssigned <- 0}
  percentage = round(overAssigned*100/total, digits=0)
  print(paste("Of the", total, "regions in", paste0(I,","), overAssigned, paste0("(", percentage,"%)"), "were associated with more than", maxGenesPerPeak, "genes."))
  }
```

# Plots: Distances to TSS

```{r}
for (I in names(pg)){
  dist = pg[[I]]
  dist = dist[,c("geneName", "peakName","fromTSS")]
  dist = dist[dist$fromTSS<3000 & dist$fromTSS>-3000,]
  hist(dist$fromTSS, breaks=50, add=F, col=grey(0.7),
       main=paste("Distance between", I, "summits and TSS"),
       xlab="Distance to TSS")
  mtext("negative values indicate that the peak is upstream of the gene", side=1, line=2)
  legend(x="topleft", legend=c("all genes", "closest gene"), 
         col=c(grey(0.7),grey(0.4)), pch=15, bty='n')
  
  dist = dist[order(abs(dist$fromTSS)),]
  dist = dist[!duplicated(dist$peakName),]
  hist(dist$fromTSS, breaks=50, add=T, col=grey(0.4))
  
  abline(v=0, lwd=2)
  text("TSS", x=0, y=par('usr')[4], adj=c(.5,0), xpd=T)
}
```

# Write Files

## main output

AllSites requires some modification so the column names are accurate.
```{r}
names(pg[["AllSites"]])[2:4] = c("SuperSummit", "foldEnrichment", "center")
pg[["AllSites"]] = pg[["AllSites"]][,c(1,2,4:8)] #ditch the foldEnrichment column
#
names(pg[["BAsummits"]])[2:4] = c("SuperSummit", "foldEnrichment", "center")
pg[["BAsummits"]] = pg[["BAsummits"]][,c(1,2,4:8)] #ditch the foldEnrichment column
```

Write out the summit to gene relationships.
```{r}
w=which(names(pg[["AllSites"]])=="description")
#
fname = "results/GeneAssignments_AllSites.txt"
write.table(pg[["AllSites"]], file=fname, sep='\t', row.names=F, quote=w) #use quotes on description column
#
fname = "results/GeneAssignments_BAsummits.txt"
write.table(pg[["BAsummits"]], file=fname, sep='\t', row.names=F, quote=w) #use quotes on description column
```

Write out a single file with the peak to gene relationships.
```{r write peak to gene}
AllPeaks = pg[[samples[1]]][0,] # set up empty data frame just like the peaks data frames.

for (sample in samples){
  AllPeaks = rbind(AllPeaks, pg[[sample]])
}
AllPeaks = merge(AllPeaks, macsAll[,c("name", "sample")], by.x="peakName", by.y="name")

w=which(names(AllPeaks)=="description")
fname.peaks = paste0("results/peak2gene_", upstream, "bp_minQ", minPeakQ,".tsv")
write.table(AllPeaks, file=fname.peaks, sep='\t', row.names=F, quote=w) #use quotes on description column
system(paste("gzip", fname.peaks))
fname.peaks
```

This will be where we pick up the process for calling target genes.

## write list of unassigned peaks

Write the unassigned peaks to a file to look at later.  Make it a bed file to visualize in IGB.  We may be able to find RNA-seq data that indicates novel genes that are closer to the unassigned peaks, or we may be able to make a case for longer range affects.  Or we could use these peaks to represent erroneous binding, or false positives. If we want to, we can re-introduce these peaks later by assigning them to genes based on wider criteria.
```{r unassigned peaks}
unassigned = pg[samples]
sampleColors = getSampleColors() # see projectVariables
for (I in samples){
  bed=macs[[I]][,bednames]
  unassigned[[I]] = bed[!(bed$name %in% pg[[I]]$peakName),]
  unassigned[[I]]$itemRGB = sampleColors[I]
  unas = nrow(unassigned[[I]])
  total = nrow(macs[[I]])
  percentage = round(unas*100/total, digits=0)
  print(paste("Of the", total, "regions in", paste0(I,","), unas, paste0("(", percentage,"%)"), "were not assigned to any gene."))
  }

# Put all of these together in one file
df = unassigned[[1]][0,]
for (I in names(unassigned)){
  df=rbind(df, unassigned[[I]])
}
fname.unassigned = "results/UnassignedPeaks.bed"
write.bed(df, fname.unassigned)
rm(unassigned)
rm(df)
```

# Conclusion

We have associated genes with peaks and we have calculated the distance between them.

We have written this information to a file, `r fname.peaks`, which contains the associations for all samples.  Separate files, "results/GeneAssignment_" files, have the same information for the summit regions (AllSites and BAsummits). 

For each association we have noted the distance from the summit of the peak (or center of the super summit) to the gene transcription start site (TSS). Other information about the peaks and the genes has been included from the original input files for convenience.  In cases where variants of a gene have different TSSs, we used the most upstream site. Negative values in the "fromTSS" column indicate that the summit is upstream of the gene.

There are hundreds of peaks that we have not assigned to any gene. These make up only a few percent of each sample so we will ignore them for now.  They can be examined at a later time using the file `r fname.unassigned`.

# Limitations

We have not considered any associations where the peak is more than `r upstream/1000`kb away from the gene.  We don't really know how ARR10 influences its targets or if it has long-range influence.  We are making the assumption that this range catches at least the majority of interactions. Based on the frequencies of summits surrounding the TSS, it looks this assumption is sound.

**Session Info**
```{r housekeeping}
sessionInfo()
```

**Functions**

Show the code for the functions that have been used in this mark down but defined in separate documents.
```{r}
read.bed
getGeneRange
truncateTranscriptID
write.bed
geneAssignFast
```