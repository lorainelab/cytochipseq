Peak to Gene Assignment - results
=================================

* * *

### ClosestGene_1500bp_AllPeaksQ10.txt
Made by Exploratory-Peak-ClosestGene.Rmd

For every peak scoring 10 or better, reports the closest gene within 1500 bases. Genes reported if they are downstream of the summit within 1500 bases of the peak summit. 

* * *

### ConsensusPeaks_ClosestGene_limit1500bp.txt
Made by ConsensusPeaksToClosestGenes.Rmd

Each BApeak is linked to the nearest downstream gene on each strand.

"Downstream" is meaningfull for genes, but peaks have no direction. Each peak is associated with one gene (the closest downstream gene) on each strand. Additionally, the peak may be associated with any gene it is internal to.  So each peak is associated with exactly 2 or 3 genes.

* * *

### GeneAssignments
Made by Exploratory-Peak-Gene_assignment.Rmd

* * *

### peak2gene_3000bp_minQ10.tsv.gz
Made by Exploratory-Peak-Gene_assignment.Rmd 

For every peak scoring 10 or better, reports all genes located w/in 3000 bases upstream and 1000 bases downstream.

* * *

### top100.tsv
output of TopGenes.Rmd

* * *

### UnassignedPeaks.bed
Made by Peak-Gene_assignment.Rmd

Peaks without nearby genes w/in 3000 bases upstream and 1000 bases downstream.